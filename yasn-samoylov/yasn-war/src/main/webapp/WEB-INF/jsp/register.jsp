<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Register</title>
	<%--<link href="<c:url value="/css/wall.css" />" rel="stylesheet">--%>
	<link href="<c:out value="${pageContext.request.contextPath}"/>/css/style.css" rel="stylesheet">
	<link href="<c:out value="${pageContext.request.contextPath}"/>/css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link href="<c:out value="${pageContext.request.contextPath}"/>/css/fileinput.css" rel="stylesheet"/>
</head>
<body>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<div class="container" style="margin-top: 60px" id="page"> <!--width: 900px-->
	<div class="row">
		<%@ include file="/WEB-INF/jsp/leftcolumn.jsp" %>
		<div class="col-sm-9" id="page-content">

			<form class="form-horizontal" id="regform" action="register" method="POST">
				<fieldset>
					<%--@elvariable id="currCity" type="com.getjavajob.lesson15.samoylove.yasn.entity.City"--%>
					<%--@elvariable id="birthCity" type="com.getjavajob.lesson15.samoylove.yasn.entity.City"--%>
					<%--@elvariable id="savedFields" type="com.getjavajob.lesson15.samoylove.yasn.entity.User"--%>
					<%--@elvariable id="regErrorMessage" type="texts"--%>
					<%--@elvariable id="allCountries" type="java.util.List"--%>
					<%--@elvariable id="birthCountryId" type="com.getjavajob.lesson15.samoylove.yasn.entity.Country"--%>
					<%--@elvariable id="currCountryId" type="com.getjavajob.lesson15.samoylove.yasn.entity.Country"--%>
					<legend>Register form</legend>
					<%--<span style="color: red">
					<c:out value="${regErrorMessage}"/></span>--%>

					<%--<div class="form-group">
						<label class="col-lg-2 col-sm-3 col-xs-4 control-label">Your name*</label>

						<div class="col-sm-5 col-xs-6">
							<input class="form-control"/>
						</div>
					</div>--%>

					<div class="form-group">
						<label for="name" class="col-lg-2 col-sm-3 col-xs-4 control-label">Your name*</label>

						<div class="col-sm-5 col-xs-6">
							<input class="form-control" id="name" name="name"
								   value="<c:out value="${savedFields.name}"/>"/>
						</div>
					</div>

					<div class="form-group">
						<label for="nickname" class="col-lg-2 col-sm-3 col-xs-4 control-label">Nickname</label>

						<div class="col-sm-5 col-xs-6">
							<input class="form-control" id="nickname" name="nickname"
								   value="<c:out value="${savedFields.name}"/>"/>
						</div>
					</div>

					<div class="form-group">
						<label for="password" class="col-lg-2 col-sm-3 col-xs-4 control-label">New password*</label>

						<div class="col-lg-5 col-sm-5 col-xs-4">
							<input class="form-control" type="password" id="password" name="password"/>
						</div>
						<div class="col-lg-5 col-sm-4 col-xs-4">
							<input class="form-control" type="password" id="password2" name="password2"
								   title="one more"/>
						</div>
					</div>


					<div class="form-group">
						<label for="mf" class="col-lg-2 col-sm-3 col-xs-4 control-label">I am</label>

						<div class="btn-group" data-toggle="buttons" id="mf">
							<%--active--%>
							<label class="btn btn-info <c:if test="${savedFields.gender}">active</c:if>">Male
								<input type="radio" id="m" name="sex" value="true">
							</label>
							<label class="btn btn-danger <c:if test="${!savedFields.gender}">active</c:if>">Female
								<input type="radio" id="f" name="sex" value="false">
							</label>
						</div>
					</div>


					<div class="form-group">
						<label for="birth" class="col-lg-2 col-sm-3 col-xs-4 control-label"><strong>Birth date</strong></label>

						<div class="col-lg-4 col-sm-3 col-xs-4">
							<input type="date" class="form-control" id="birth" name="birth"
								   value="<c:out value="${savedFields.birth}"/>"/>
						</div>
					</div>

					<div class="form-group">
						<label for="email" class="col-lg-2 col-sm-3 col-xs-4 control-label">Email*</label>

						<div class="col-sm-5 col-xs-6">
							<input class="form-control" type="email" id="email" name="email"
								   value="<c:out value="${savedFields.email}"/>"/>
						</div>
					</div>


					<div class="form-group">
						<label for="bcountry" class="col-lg-2 col-sm-3 col-xs-4 control-label">Birth Country</label>

						<div class="col-lg-4 col-sm-3 col-xs-6">
							<select name="" id="bcountry" class="form-control">
								<option value=""></option>

								<c:forEach var="country" items="${allCountries}" varStatus="ctr">
									<option id="<c:out value="${country.id}"/>" value="<c:out value="${country.id}"/>">
										<c:out
												value="${country.name}"/></option>
								</c:forEach>
								<script>
									var countryList = document.getElementById("bcountry");
									countryList.value = "<c:out value="${birthCountryId}"/>";
								</script>
							</select>
						</div>
						<%--</div>--%>

						<%--<div class="form-group">--%>
						<label for="bcity" class="col-lg-2 col-sm-3 col-xs-4 control-label">Birth City</label>

						<div class="col-lg-4 col-sm-3 col-xs-6">
							<select name="bcity" id="bcity" class="form-control">
								<option value="<c:out value="${birthCity.id}"/>">
									<c:out value="${birthCity.name}"/>
								</option>
							</select>
							<br>
						</div>
					</div>

					<div class="form-group">
						<label for="ccountry" class="col-lg-2 col-sm-3 col-xs-4 control-label">Current Country</label>

						<div class="col-lg-4 col-sm-3 col-xs-6">
							<select name="" id="ccountry" class="form-control">
								<option value=""></option>
								<c:forEach var="country" items="${allCountries}" varStatus="ctr">
									<option value="<c:out value="${country.id}"/>"><c:out
											value="${country.name}"/></option>
								</c:forEach>
								<script>
									var countryList = document.getElementById("bcountry");
									countryList.value = <c:out value="${currCountryId}"/>;
								</script>
							</select>
						</div>
						<%--</div>--%>

						<%--<div class="form-group">--%>
						<label for="ccity" class="col-lg-2 col-sm-3 col-xs-4 control-label">Current City</label>

						<div class="col-lg-4 col-sm-3 col-xs-6">
							<select class="form-control" name="ccity" id="ccity">
								<option value="<c:out value="${currCity.id}"/>">
									<c:out value="${currCity.name}"/>
								</option>
							</select>
						</div>
					</div>


					<div class="form-group">
						<label for="profile" class="col-lg-2 col-sm-3 col-xs-4 control-label">Who can view your
							profile?</label>

						<div class="col-lg-4 col-sm-3 col-xs-6">
							<select class="form-control" name="" id="profile">
								<option value=""></option>
								<c:forEach var="country" items="${allCountries}" varStatus="ctr">
									<option value="<c:out value="${country.id}"/>"><c:out
											value="${country.name}"/></option>
								</c:forEach>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="ava" class="col-lg-2 col-sm-3 col-xs-4 control-label">Select avatar</label>

						<div class="col-lg-5 col-md-6 col-sm-8 col-xs-6">
							<input id="ava" type="file" multiple="false" class="file-loading form-control">
						</div>
					</div>

					<div class="form-group">
						<div class="g-recaptcha" data-sitekey="6LcTaQwTAAAAAFytiO9tD2P2ibF2p-s3FK57lcUA"></div>
					</div>


					<label for="submit" class="col-sm-3 col-xs-3 control-label"></label>
					<input class="btn btn-primary col-sm-6 col-xs-6" type="submit" id="submit" name="Register"
						   value="Register"/>
				</fieldset>
			</form>

		</div>
		<!--end page-content-->
		<%@ include file="/WEB-INF/jsp/footer.jsp" %>
		<script src="<c:out value="${pageContext.request.contextPath}"/>/js/edit.js"></script>

		<%--file input plugin--%>
		<script src="<c:out value="${pageContext.request.contextPath}"/>/js/fileinput.js"></script>
		<script src="<c:out value="${pageContext.request.contextPath}"/>/js/fileinput_locale_ru.js"></script>
</body>
</html>