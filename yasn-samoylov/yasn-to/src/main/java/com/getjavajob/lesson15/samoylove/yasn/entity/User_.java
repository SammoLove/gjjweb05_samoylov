package com.getjavajob.lesson15.samoylove.yasn.entity;

import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

@StaticMetamodel(User.class)
public class User_ {
    public static volatile SingularAttribute<User, Integer> id;
    public static volatile SingularAttribute<User, String> name;
    public static volatile SingularAttribute<User, String> nickname;
    public static volatile SingularAttribute<User, Boolean> gender;
    public static volatile SingularAttribute<User, String> email;
    public static volatile SingularAttribute<User, Date> birth;
    public static volatile SingularAttribute<User, Integer> birthLocation;
    public static volatile SingularAttribute<User, Integer> currLocation;
    public static volatile SingularAttribute<User, String> password;
    public static volatile SingularAttribute<User, byte[]> avatar;
    public static volatile SetAttribute<User, User> friends;
}