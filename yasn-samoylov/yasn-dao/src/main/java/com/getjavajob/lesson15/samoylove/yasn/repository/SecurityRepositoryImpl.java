package com.getjavajob.lesson15.samoylove.yasn.repository;

import com.getjavajob.lesson15.samoylove.yasn.entity.SecuritySetting;
import org.springframework.stereotype.Repository;

@Repository
public class SecurityRepositoryImpl extends AbstractCommonRepository<SecuritySetting> implements SecurityRepository {
    @Override
    protected Class<SecuritySetting> getEntityClass() {
        return SecuritySetting.class;
    }
}