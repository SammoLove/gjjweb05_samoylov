package com.getjavajob.training.web05.samoylove.lesson01;

import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

public class Task5Test {

    @Test
    public void testUnionSets() {
        Set<Integer> set1 = Sets.newHashSet(1, 2, 3, 4);
        Set<Integer> set2 = Sets.newHashSet(3, 4, 5, 6);
        Set<Integer> rightUnion = Sets.newHashSet(1, 2, 3, 4, 5, 6);

        Set result = Task5.unionSets(set1, set2);

        Assert.assertEquals(rightUnion, result);
    }
}