package com.getjavajob.training.web05.samoylove.lesson09;

import com.google.common.annotations.VisibleForTesting;

import java.io.*;
import java.util.*;

public class Task implements WordCounter {
    static final int THREADS = 5;
    final Object globalLock = new Object();
    Map<String, Integer> words = new TreeMap<>();
    //NavigableMap<String, Integer> words = new ConcurrentSkipListMap<>();
    Queue<File> textFiles = new LinkedList<>();

    @Override
    public void countWords(final String dirName) {
        File path = new File(dirName);
        if (!path.isDirectory())
            return;
        FilenameFilter txtFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                String lowercaseName = name.toLowerCase();
                return lowercaseName.endsWith(".txt");
            }
        };
        Collections.addAll(textFiles, path.listFiles(txtFilter));

        final Thread[] threads = new Thread[THREADS];
        for (int i = 0; i < THREADS; i++) {
            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (!textFiles.isEmpty()) {
                        parseWords(textFiles.poll());
                    }
                }
            });
        }
        for (int i = 0; i < THREADS; i++) {
            threads[i].start();
        }
        for (int i = 0; i < THREADS; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        words.remove("-");
        writeWordsToFile(dirName);
    }

    private void parseWords(final File thisFile) {
        try (BufferedReader br = new BufferedReader(new FileReader(thisFile))) {
            String currentLine;
            while ((currentLine = br.readLine()) != null) {
                handleLine(currentLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @VisibleForTesting
    protected Map<String, Integer> handleLine(String line) {
        String[] s = line.split(" ");
        for (String s1 : s) {
            String s2 = s1.replaceAll("[^a-zA-Zа-яА-Я-\\s]", "").trim().toLowerCase();
            if (s2.length() > 0) {
                synchronized (globalLock) { //is not used in case words is ConcurrentSkipListMap
                    if (words.containsKey(s2)) {
                        words.put(s2, words.get(s2) + 1);
                    } else {
                        words.put(s2, 1);
                    }
                }
            }
        }
        return words;
    }

    private void writeWordsToFile(String dirName) {
        try (BufferedWriter br = new BufferedWriter(new FileWriter(new File(dirName + "/words.txt")))) {
            Set<Map.Entry<String, Integer>> entries = words.entrySet();
            for (Map.Entry<String, Integer> entry : entries) {
                br.write(entry.getKey() + " => " + entry.getValue());
                br.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}