package com.getjavajob.lesson15.samoylove.yasn.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "usert")
public class User extends TO {
    @Id
    @SequenceGenerator(name = "usert_id_seq", sequenceName = "usert_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usert_id_seq")
    private int id;

    @NotNull
    private String name;
    private String nickname;
    private Boolean gender;

    @NotNull
    private String email;

    private Date birth;

    @Column(name = "birth_location")
    private int birthLocation;

    @Column(name = "curr_location")
    private int currLocation;

    @NotNull
    private String password;

    @ManyToMany
    @JoinTable(name = "friends",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "friend_id"))
    private Set<User> friends;

    //@MapsId() //с ним ОШИБКА: колонка user0_.avatar_user_id не существует. Какой нахрен "avatar_user_id"?
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Avatar avatar;

    @Transient
    private int age;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public int getBirthLocation() {
        return birthLocation;
    }

    public void setBirthLocation(int birthLocation) {
        this.birthLocation = birthLocation;
    }

    public int getCurrLocation() {
        return currLocation;
    }

    public void setCurrLocation(int currLocation) {
        this.currLocation = currLocation;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        Calendar dob = Calendar.getInstance();
        if (birth != null) {
			dob.setTime(birth);
			Calendar today = Calendar.getInstance();
			int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
			if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
				age--;
			} else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
					&& today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
				age--;
			}
			return age;
		}
		return 0;
    }

    public void setAge(int age) {
        this.age = getAge();
    }

    public Set<User> getFriends() {
        return friends;
    }

    public void setFriends(Set<User> friends) {
        this.friends = friends;
    }

    public Avatar getAvatar() {
        return avatar;
    }

    public void setAvatar(Avatar avatar) {
        this.avatar = avatar;
    }
}