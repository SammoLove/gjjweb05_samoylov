package com.getjavajob.lesson15.samoylove.yasn.web;

import com.getjavajob.lesson15.samoylove.yasn.entity.User;
import com.getjavajob.lesson15.samoylove.yasn.services.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
public class LoginController {
    private final LoginService loginService;
    private final UserInfoSessionStorage userInfoSessionStorage;

    @Autowired
    public LoginController(LoginService loginService, UserInfoSessionStorage userInfoSessionStorage) {
        this.loginService = loginService;
        this.userInfoSessionStorage = userInfoSessionStorage;
    }

    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public String getLoginPage() {
        return "login";
    }


    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public ModelAndView loginUser(@RequestParam("email") String email,
                                  @RequestParam("password") String password,
                                  @RequestParam(name = "shortSession", required = false) Boolean shortSession,
                                  @RequestParam(name = "returnUrl", required = false) String returnUrl,
                                  HttpServletRequest req, HttpServletResponse resp,
                                  final HttpSession session) {

        User user = loginService.checkPassword(email, password);
        if (user != null && user.getId() > 0) { //if use was found
            session.removeAttribute("authFiltered");

            if (shortSession == null) { //save cookies only if not checked short session
                final Cookie cookie = new Cookie("userId", String.valueOf(user.getId()));
                resp.addCookie(cookie);
            }
            userInfoSessionStorage.setUserId(user.getId());
            userInfoSessionStorage.setUserName(user.getName());

            if (returnUrl.isEmpty() || returnUrl.equals("/logout")) {
                returnUrl = "/wall/id" + user.getId();
            }
            return new ModelAndView("redirect:" + returnUrl, null);
        } else { // if user was not found - return to login page
            resp.setCharacterEncoding("UTF-8");
            req.setAttribute("wrong", Boolean.TRUE); // передать через errors?
            Map<String, String> errors = new HashMap<>();
            errors.put("password", "incorrect email or password");
            errors.put("returnUrl", returnUrl);
            return new ModelAndView("login", errors);
        }
    }


    @RequestMapping(path = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest req, HttpServletResponse resp) {
        eraseCookie(req, resp);
        req.getSession().invalidate();
        return "redirect:/login";
    }

    private void eraseCookie(HttpServletRequest req, HttpServletResponse resp) {
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                cookie.setValue("");
                cookie.setPath("/");
                cookie.setMaxAge(0);
                resp.addCookie(cookie);
            }
        }
    }
}