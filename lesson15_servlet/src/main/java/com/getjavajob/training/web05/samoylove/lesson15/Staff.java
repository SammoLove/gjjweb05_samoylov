package com.getjavajob.training.web05.samoylove.lesson15;

import com.getjavajob.training.web05.samoylove.lesson13.ContactDAO;
import com.getjavajob.training.web05.samoylove.lesson13.ContactsList;

public class Staff {
    public static void main(String[] args) throws ClassNotFoundException {
        ContactDAO contactDAO = new ContactDAO();
        final ContactsList search1 = contactDAO.search(null);
        final ContactsList search2 = contactDAO.search("Полянская");
        System.out.println(search1.toString() + search2);
    }
}