package com.getjavajob.training.web05.samoylove.lesson01;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class Task1Test {

    @Test
    public void testReverseList() {
        //String[] exampleArray = {};
        List<String> exampleList = Arrays.asList("23", "", "cdef", "-");
        List<String> rightResultList = Arrays.asList("32", "", "fedc", "-");

        List<String> reversedList = Task1.reverseList(exampleList);

        Assert.assertEquals(rightResultList, reversedList);
    }
}