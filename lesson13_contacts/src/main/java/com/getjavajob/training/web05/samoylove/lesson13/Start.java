package com.getjavajob.training.web05.samoylove.lesson13;

import java.util.Scanner;

public class Start {
    private static ContactDAO contactDAO;

    public static void main(String[] args) {
        contactDAO = new ContactDAO();
        String entered;
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.print("> ");
            entered = scanner.nextLine();
        } while (handleCommand(entered));
    }


    protected static boolean handleCommand(String entered) {
        boolean notTimeToExit = true;
        if (entered.equals("search")) {
            System.out.println(contactDAO.search(null));


        } else if (entered.startsWith("search")) {
            System.out.println(contactDAO.search(entered.substring(7)));


        } else if (entered.startsWith("add")) {
            String[] params = entered.split(" ");

            if (entered.indexOf("phone") > 0) {
                int prim = 0;
                if (entered.indexOf("primary") > 0) {
                    prim = 1;
                }
                try {
                    contactDAO.addPhone(true, Integer.parseInt(params[2 + prim]), params[3 + prim], params[4 + prim]);
                } catch (NumberFormatException e) {
                    System.err.println("Wrong input!");
                }


            } else { //add not primary phone
                if (params.length == 4) {
                    contactDAO.add(params[1], params[2], params[3]);
                } else {
                    System.err.println("Wrong input!");
                }
            }


        } else if (entered.startsWith("delete")) {
            try {
                contactDAO.delete(Integer.parseInt(entered.substring(7)));
            } catch (NumberFormatException e) {
                System.err.println("Wrong input!");
            }


        } else if (entered.startsWith("set primary")) {
            String[] params = entered.split(" ");
            try {
                if (params.length == 4) {
                    contactDAO.setPrimary(Integer.parseInt(params[2]), Integer.parseInt(params[3]));
                } else {
                    System.err.println("Wrong input!");
                }
            } catch (NumberFormatException e) {
                System.err.println("Wrong input!");
            }


        } else if (entered.startsWith("exit")) {
            notTimeToExit = false;


        } else {
            System.out.println("Wrong command. Repeat input.");
        }
        return notTimeToExit;
    }
}