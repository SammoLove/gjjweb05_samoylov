package com.getjavajob.lesson15.samoylove.yasn.web;

import com.getjavajob.lesson15.samoylove.yasn.entity.Interlocutor;
import com.getjavajob.lesson15.samoylove.yasn.entity.Message;
import com.getjavajob.lesson15.samoylove.yasn.entity.User;
import com.getjavajob.lesson15.samoylove.yasn.services.MessageService;
import com.getjavajob.lesson15.samoylove.yasn.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/msgs")
public class MessageController {
    private MessageService messageService;
    private UserService userService;
    private UserInfoSessionStorage userInfoSessionStorage;

    @Autowired
    public MessageController(MessageService messageService, UserService userService, UserInfoSessionStorage userInfoSessionStorage) {
        this.messageService = messageService;
        this.userService = userService;
        this.userInfoSessionStorage = userInfoSessionStorage;
    }

    @RequestMapping(path = {"", "/"}, method = RequestMethod.GET)
    public ModelAndView messagesPage() {
        final Map<String, Object> model = new HashMap<>();

        final User loggedUser = userService.read(userInfoSessionStorage.getUserId());
        final List<Interlocutor> interlocutors = messageService.getInterlocutors(loggedUser);
        model.put("interlocutors", interlocutors);

        // return only interlocutors list
        return new ModelAndView("msgs", model);
    }

    @RequestMapping(path = "/getCorrespondence{interlocutorId}-{pageNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    List<Message> dialogTab(@PathVariable Integer interlocutorId, @PathVariable Integer pageNumber) {
        final User loggedUser = userService.read(userInfoSessionStorage.getUserId());
        Interlocutor interlocutor = new Interlocutor();
        interlocutor.setId(interlocutorId);
        return messageService.getCorrespondence(loggedUser, interlocutor);
    }

    @RequestMapping(path = "/unreadCount", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    Integer getUnreadCount() {
        final User loggedUser = userService.read(userInfoSessionStorage.getUserId());
        return messageService.getUnreadCount(loggedUser);
    }

    /**
     * Input is numbers of messages was read to mark its as read
     */
    @RequestMapping(path = "/markAsRead{interlocutorId}", method = RequestMethod.POST)
    public void markAsRead(@PathVariable Integer interlocutorId) {

        //todo implement transfer of messages id that was read
        final User loggedUser = userService.read(userInfoSessionStorage.getUserId());
        Interlocutor interlocutor = new Interlocutor();
        interlocutor.setId(interlocutorId);
        messageService.markAsRead();
    }
}