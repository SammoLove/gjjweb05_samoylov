package com.getjavajob.training.web05.samoylove.lesson16;

public class ListSorter<T extends Comparable<T>> {
    private DoublyLinkedList<T> dll;

    public DoublyLinkedList<T> mergeSort(DoublyLinkedList<T> dll) {
        this.dll = dll;
        mSort(this.dll, 0, dll.getSize() - 1);
        return this.dll;
    }

    private void mSort(DoublyLinkedList<T> dll, int lo0, int hi0) {
        int lo = lo0;
        if (lo >= hi0) { // exit condition of the recursion
            return;
        }

        int mid = (lo + hi0) >> 1; // split
        mSort(dll, lo, mid);
        mSort(dll, mid + 1, hi0);

        // merge two sorted lists
        int endLo = mid;
        int startHi = mid + 1;
        while ((lo <= endLo) && (startHi <= hi0)) {
            if (dll.get(lo).compareTo(dll.get(startHi)) < 0) {
                lo++;
            } else { // i.e. dll.lo >= dll.startHi
                T tmp = dll.get(startHi);
                for (int k = startHi - 1; k >= lo; k--) {
                    dll.set(k + 1, dll.get(k));
                }
                dll.set(lo, tmp);
                lo++;
                endLo++;
                startHi++;
            }
        }
    }

}
