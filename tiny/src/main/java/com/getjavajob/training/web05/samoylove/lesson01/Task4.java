package com.getjavajob.training.web05.samoylove.lesson01;

// Trying to work with the Guava - an external library

import com.google.common.collect.Sets;

import java.util.TreeSet;

public class Task4 {
    public static <T extends Number & Comparable<T>> int findMinMaxDif(Iterable<T> elements) {
        if (elements != null) {
            TreeSet<T> elementsTree = Sets.newTreeSet(elements);
            return elementsTree.last().intValue() - elementsTree.first().intValue();
        }
        return -1;
    }
}