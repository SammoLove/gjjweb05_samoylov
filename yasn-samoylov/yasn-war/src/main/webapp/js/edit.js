$(document).ready(function (){
	$("#ava").fileinput({showCaption: false});
});

var object1 = document.getElementById("bcountry");
object1.addEventListener("change", function (){
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	xmlhttp.onreadystatechange = function (){
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("bcity").innerHTML = xmlhttp.responseText;
		}
	};
	xmlhttp.open("GET", "/getCities/" + this.value, true);
	xmlhttp.send();
});

var object2 = document.getElementById("ccountry");
object2.addEventListener("change", function (){
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	xmlhttp.onreadystatechange = function (){
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("ccity").innerHTML = xmlhttp.responseText;
		}
	};
	xmlhttp.open("GET", "/getCities/" + this.value, true);
	xmlhttp.send();
});