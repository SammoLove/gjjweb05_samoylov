package com.getjavajob.lesson15.samoylove.yasn.web;

import com.getjavajob.lesson15.samoylove.yasn.entity.City;
import com.getjavajob.lesson15.samoylove.yasn.entity.MultipartFileContainer;
import com.getjavajob.lesson15.samoylove.yasn.entity.SecuritySetting;
import com.getjavajob.lesson15.samoylove.yasn.entity.User;
import com.getjavajob.lesson15.samoylove.yasn.services.LocationService;
import com.getjavajob.lesson15.samoylove.yasn.services.SecurityService;
import com.getjavajob.lesson15.samoylove.yasn.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/wall")
public class WallController {
    @Autowired
    UserService userService;
    @Autowired
    LocationService locationService;
    @Autowired
    SecurityService securityService;
    @Autowired
    UserInfoSessionStorage userInfoSessionStorage;

    @RequestMapping(path = {"/", ""}, method = RequestMethod.GET)
    public String getDefaultWall() {
        final int loggedUserId = userInfoSessionStorage.getUserId();
        if (loggedUserId > 0) {
            return "redirect:/wall/id" + loggedUserId;
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(path = "/{userNick}", method = RequestMethod.GET)
    public ModelAndView getWallByNickname(@PathVariable String userNick) {
        User wallUser = userService.searchByNickname(userNick);
        if (wallUser != null) {
            return privateFiltered(wallUser);
        } else {
            return showErrorPage("Error. Such nickname not found.");
        }
    }

    @RequestMapping(path = "/id{wallId}", method = RequestMethod.GET)
    public ModelAndView getWallById(@PathVariable Integer wallId, HttpServletRequest req) {
        //@RequestParam(value = "errorMsg", required = false) String errorMsg //для вывода сообщений об ошибке
        final User wallUser = userService.read(wallId); //проверим есть ли пользователь с таким id в БД
        if (wallUser != null) { //если да, проверим есть ли у него ник
            if (!wallUser.getNickname().isEmpty()) { //если ник есть, отредиректим на /wall/nickname
                return new ModelAndView("redirect:/wall/" + wallUser.getNickname());
            }
        } else {
            return showErrorPage("User with id " + wallId + " was not found!");
        }
        return privateFiltered(wallUser);
    }

    private ModelAndView privateFiltered(User wallUser) {
        final int loggedUserId = userInfoSessionStorage.getUserId();
        final SecuritySetting securitySetting = securityService.read(wallUser.getId());
        AccessLevel accessLevel;
        if (securitySetting != null) {
            accessLevel = AccessLevel.getType(securitySetting.getProfileAccessLevel());
        } else {
            accessLevel = AccessLevel.EVERYONE; //access level by default (if no DB entry in 'private_set' for this user)!
        }
        // todo ban checks here
        switch (accessLevel) {
            case EVERYONE:
                return showWall(wallUser); //no conditions
            case REGISTERED:
                final Boolean registered = (loggedUserId > 0);
                return showWallForRegistered(wallUser, "not registered", registered);
            case FRIENDS:
                return showWallForFriends(wallUser, "non-friend", loggedUserId);
            case NONE:
                return showAccessError("all");
            default:
                return showWall(wallUser);
        }
    }

    private ModelAndView showErrorPage(final String errorMsg) {
        Map<String, ?> model = new HashMap<String, Object>() {{
            put("errorMsg", errorMsg);
            put("multipartFileContainer", new MultipartFileContainer());
        }};
        return new ModelAndView("errorPage", model);
    }

    private ModelAndView showAccessError(final String who) {
        return showErrorPage("The user has denied access for " + who + " users");
    }

    private ModelAndView showWallForRegistered(User requestedWallUser, String errorMsg, Boolean registered) {
        if (registered) {
            return showWall(requestedWallUser);
        } else {
            return showAccessError(errorMsg);
        }
    }

    private ModelAndView showWallForFriends(User wallUser, String errorMsg, int loggedUserId) {
        if (loggedUserId > 0) {
            if (userService.isFriend(loggedUserId, wallUser.getId())) {
                return showWall(wallUser);
            }
        }
        return showAccessError(errorMsg);
    }

    private ModelAndView showWall(final User wallUser) {
        final City bornCity = locationService.readCity(wallUser.getBirthLocation());
        final City liveCity = locationService.readCity(wallUser.getCurrLocation());

        final Map<String, ?> model = new HashMap<String, Object>() {{
            put("wallUser", wallUser);
            //model.put("errorMsg", errorMsg);
            put("bornCity", bornCity);
            put("liveCity", liveCity);
            put("multipartFileContainer", new MultipartFileContainer());
        }};
        return new ModelAndView("wall", model);
    }
}