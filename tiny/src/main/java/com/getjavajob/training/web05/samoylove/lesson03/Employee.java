package com.getjavajob.training.web05.samoylove.lesson03;

import java.util.Objects;

public class Employee extends Person {
    private String position;

    public Employee(String name, String homeAddress, String position) {
        super(name, homeAddress);
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        Employee employee = (Employee) o;
        return this == o || getName().equals(employee.getName()) && getHomeAddress().equals(employee.getHomeAddress()) && position.equals(employee.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getPosition());
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Employee{");
        sb.append("name='").append(getName()).append('\'');
        sb.append(", homeAddress='").append(getHomeAddress()).append('\'');
        sb.append(", position='").append(position).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public Employee clone() throws CloneNotSupportedException {
        Employee copy = (Employee) super.clone();
        copy.position = new String(this.position);
        return copy;
    }
}
