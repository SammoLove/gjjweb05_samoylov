package com.getjavajob.lesson15.samoylove.yasn.entity;

import org.springframework.web.multipart.MultipartFile;

public class MultipartFileContainer {

    MultipartFile multipartFile;

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }
}