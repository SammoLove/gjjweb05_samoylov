CREATE TABLE usert (
  id             SERIAL      NOT NULL PRIMARY KEY,
  name           VARCHAR(30) NOT NULL,
  nickname       VARCHAR(30),
  gender         BOOL,
  email          VARCHAR(30) NOT NULL,
  birth          DATE,
  birth_location INT,
  curr_location  INT,
  password       VARCHAR(10) NOT NULL
);

CREATE TABLE avatar (
  user_id    INT UNIQUE NOT NULL PRIMARY KEY,
  mime_type  VARCHAR(30) NOT NULL,
  image_file BYTEA NOT NULL,
  FOREIGN KEY (user_id) REFERENCES usert (id) ON DELETE CASCADE
);

CREATE TABLE country (
  id   SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(20)
);

CREATE TABLE city (
  id         SERIAL NOT NULL PRIMARY KEY,
  name       VARCHAR(20),
  country_id INT,
  FOREIGN KEY (country_id) REFERENCES country (id)
);
ALTER TABLE usert ADD FOREIGN KEY (birth_location) REFERENCES city (id);
ALTER TABLE usert ADD FOREIGN KEY (curr_location) REFERENCES city (id);

CREATE TABLE level (
  id    INT PRIMARY KEY,
  descr VARCHAR(10)
);

CREATE TABLE private_set (
  id               SERIAL NOT NULL PRIMARY KEY,
  user_id          INT,
  can_view_profile INT DEFAULT 0,
  can_view_freinds INT DEFAULT 0,
  can_post_private INT DEFAULT 0,
  can_post_to_wall INT DEFAULT 0,
  FOREIGN KEY (can_view_profile) REFERENCES level (id),
  FOREIGN KEY (can_view_freinds) REFERENCES level (id),
  FOREIGN KEY (can_post_private) REFERENCES level (id),
  FOREIGN KEY (can_post_to_wall) REFERENCES level (id)
);

CREATE TABLE friends (
  id        SERIAL NOT NULL PRIMARY KEY,
  user_id   INT,
  friend_id INT,
  friend    BOOL,
  FOREIGN KEY (user_id) REFERENCES usert (id) ON DELETE CASCADE,
  FOREIGN KEY (friend_id) REFERENCES usert (id) ON DELETE CASCADE
);

CREATE TABLE message (
  id             SERIAL NOT NULL PRIMARY KEY,
  body           VARCHAR(2000),
  time           TIME,
  user_id        INT,
  author_user_id INT,
  readed         BOOL,
  --   file           INT,
  priv           BOOL,
  FOREIGN KEY (user_id) REFERENCES usert (id) ON DELETE CASCADE,
  FOREIGN KEY (author_user_id) REFERENCES usert (id) ON DELETE CASCADE
  --   FOREIGN KEY (file) REFERENCES avatar (id) ON DELETE CASCADE
);

CREATE TABLE favorite (
  id      SERIAL NOT NULL PRIMARY KEY,
  user_id INT,
  fav     INT,
  FOREIGN KEY (user_id) REFERENCES usert (id) ON DELETE CASCADE,
  FOREIGN KEY (fav) REFERENCES wall_msg (id) ON DELETE CASCADE
);

CREATE TABLE interest (
  id      SERIAL NOT NULL PRIMARY KEY,
  user_id INT,
  hobby   VARCHAR(200),
  FOREIGN KEY (user_id) REFERENCES usert (id) ON DELETE CASCADE
);

CREATE TABLE setting (
  id    SERIAL NOT NULL PRIMARY KEY,
  descr VARCHAR(20)
);

CREATE TABLE user_setting (
  id        SERIAL NOT NULL PRIMARY KEY,
  user_id   INT,
  seting_id INT,
  val       VARCHAR(30),
  FOREIGN KEY (seting_id) REFERENCES setting (id) ON DELETE CASCADE,
  FOREIGN KEY (user_id) REFERENCES usert (id) ON DELETE CASCADE
);