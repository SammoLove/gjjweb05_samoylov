package com.getjavajob.training.web05.samoylove.lesson08;

import com.getjavajob.training.web05.samoylove.lesson06.Task2;

import java.io.File;
import java.util.Collection;
import java.util.Map;

import static com.getjavajob.training.web05.samoylove.lesson06.Task2.loadGoodsByDOM;

public class DOMParser implements XmlParser {
    @Override
    public Map<String, Collection<Task2.Product>> parser(final File XMLFile) {
        return loadGoodsByDOM(XMLFile);
    }
}