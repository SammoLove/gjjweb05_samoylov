package com.getjavajob.lesson15.samoylove.yasn.repository;

import com.getjavajob.lesson15.samoylove.yasn.entity.City;
import com.getjavajob.lesson15.samoylove.yasn.entity.User;
import com.getjavajob.lesson15.samoylove.yasn.entity.User_;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepository extends AbstractCommonRepository<User> {
	@PersistenceContext
	EntityManager entityManager;

	@SuppressWarnings("JpaQlInspection")
	public User findByEmail(String email) {
		final TypedQuery<User> query = entityManager.createQuery("select u from User as u where u.email=:email", User.class);
		query.setParameter("email", email);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	@SuppressWarnings("JpaQlInspection")
	public User findByNickname(String nickname) {
		final TypedQuery<User> query = entityManager.createQuery("select u from User as u where u.nickname=:nickname", User.class);
		query.setParameter("nickname", nickname);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	protected Class<User> getEntityClass() {
		return User.class;
	}

	public List<User> searchByParams(SearchParams searchParams) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
		Root<User> root = criteriaQuery.from(User.class);

		Predicate[] conditions = conditionsBuilder(criteriaBuilder, root, searchParams);

		criteriaQuery.where(conditions);
		TypedQuery<User> q = entityManager.createQuery(criteriaQuery);
		return q.getResultList();
	}

	private Predicate[] conditionsBuilder(CriteriaBuilder criteriaBuilder, Root<User> root, SearchParams searchParams) {
		List<Predicate> predicates = new ArrayList<>();
		if (searchParams.getPartName() != null) {
			predicates.add(
					criteriaBuilder.or(
							criteriaBuilder.like(
									criteriaBuilder.lower(
											root.get(User_.name)
									), searchParams.getPartName().toLowerCase() + '%'),
							criteriaBuilder.like(
									criteriaBuilder.lower(
											root.get(User_.name)
									), searchParams.getNickname().toLowerCase() + '%')));
		}

		if (searchParams.getGender() != null) {
			predicates.add(criteriaBuilder.equal(root.get(User_.gender), searchParams.getGender()));
		}
		if (searchParams.getMinAge() != null) {
			predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(User_.birth), searchParams.getMinAge()));
		}
		if (searchParams.getMaxAge() != null) {
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(User_.birth), searchParams.getMaxAge()));
		}
		if (searchParams.getBirthCountry() != null) {
			Subquery<City> subquery = criteriaBuilder.createQuery().subquery(City.class);
			Root<City> cityRoot = subquery.from(City.class);
			subquery.select(cityRoot.<City>get("country"));
			subquery.where(criteriaBuilder.equal(cityRoot.<City>get("country"), searchParams.getBirthCountry()));

			predicates.add(criteriaBuilder.in(cityRoot).value(subquery));
		}
		if (searchParams.getBirthCity() != null) {
			predicates.add(criteriaBuilder.equal(root.get(User_.birthLocation), searchParams.getBirthCity()));
		}
		if (searchParams.getCurrCountry() != null) {
			/*Subquery<City> subquery = criteriaBuilder.createQuery().subquery(City.class);
			Root<City> cityRoot = subquery.from(City.class);
			subquery.select(cityRoot.<City>get("country"));
			subquery.where(criteriaBuilder.equal(cityRoot.<City>get("country"), searchParams.getCurrCountry()));

			//Path<Object> path = from.get("compare_field"); // field to map with sub-query
			predicates.add(criteriaBuilder.in(cityRoot).value(subquery));*/
		}
		if (searchParams.getCurrCity() != null) {
			predicates.add(criteriaBuilder.equal(root.get(User_.currLocation), searchParams.getCurrCity()));
		}
		return predicates.toArray(new Predicate[predicates.size()]);
	}
}