package com.getjavajob.training.web05.samoylove.lesson08;

import com.getjavajob.training.web05.samoylove.lesson06.Task2;

import java.io.File;
import java.util.Collection;
import java.util.Map;

public interface XmlParser {
    Map<String, Collection<Task2.Product>> parser(File file);
}
