package com.getjavajob.training.web05.samoylove.lesson11;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

class Server extends Thread {
    private static int port;
    private static ConcurrentMap<String, DataOutputStream> usersStreams = new ConcurrentHashMap<>();
    private final Logger logger = LogManager.getRootLogger();
    private Socket clientSocket;
    private String currentThreadUser;

    public Server(int p) {
        port = p;
        work();
    }

    private Server(Socket clientSocket) {
        this.clientSocket = clientSocket;
        start();
    }

    //@OnlyForTests
    protected Server() {
    }

    public void work() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            while (true) {
                System.err.println("New server thread started. Waiting for a clients...");
                Socket clientSocket = serverSocket.accept();
                System.err.println();
                System.err.println("Got a client :)" + clientSocket.toString());
                new Server(clientSocket);
            }
        } catch (IOException e) {
            System.err.println("The server is already running! Closing this.");
        }
    }

    @Override
    public void run() {
        try (DataInputStream dataInputStream = new DataInputStream(clientSocket.getInputStream());
             DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream())) {

            System.err.print("getting user name... ");
            String line;
            line = dataInputStream.readUTF();
            currentThreadUser = line.substring(1);
            System.err.println("Got! It's " + currentThreadUser);
            setName(currentThreadUser); //Thread name as username
            if (usersStreams.putIfAbsent(currentThreadUser, dataOutputStream) == null) { //if it is new user broadcast about it
                for (Map.Entry<String, DataOutputStream> stream : usersStreams.entrySet()) {
                    stream.getValue().writeUTF("\n" + currentThreadUser + " just joined the chat.\n> ");
                    stream.getValue().flush();
                }
            } else {
                throw new UserAlreadyExistsException();
            }

            while (clientSocket.isConnected()) {
                line = dataInputStream.readUTF();
                System.err.println();
                System.err.println("The client just sent me this line : " + line);
                logger.info(currentThreadUser + ": " + line);

                // strange method whatAndHowReplyOn was created specially for the tests compatibility
                Map<ReplyKind, String> reply = whatAndHowReplyOn(line);

                if (reply.containsKey(ReplyKind.ULIST)) {
                    dataOutputStream.writeUTF(reply.get(ReplyKind.ULIST));
                    dataOutputStream.flush();
                } else if (reply.containsKey(ReplyKind.PRIV)) {
                    String toUser = line.substring(1, line.indexOf(" "));
                    usersStreams.get(toUser).writeUTF(reply.get(ReplyKind.PRIV));
                    usersStreams.get(toUser).flush();
                    dataOutputStream.writeUTF("> ");
                    dataOutputStream.flush();
                } else {
                    for (Map.Entry<String, DataOutputStream> stream : usersStreams.entrySet()) {
                        if (!currentThreadUser.equals(stream.getKey())) {
                            stream.getValue().writeUTF(reply.get(ReplyKind.BCAST));
                            stream.getValue().flush();
                        } else {
                            dataOutputStream.writeUTF("> ");
                            dataOutputStream.flush();
                        }
                    }
                }
            }
        } catch (UserAlreadyExistsException e) {
            e.printStackTrace();
        } catch (SocketException e) {
            System.err.println("It's lost connection with user " + currentThreadUser);
        } catch (IOException e) {
            System.err.println("User just logged out: " + currentThreadUser);
        } finally {
            usersStreams.remove(currentThreadUser);
            for (Map.Entry<String, DataOutputStream> stream : usersStreams.entrySet()) {
                try {
                    stream.getValue().writeUTF("\nUser " + currentThreadUser + " has left the chat.\n> ");
                    stream.getValue().flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.err.println("Client socket closed. Server thread closed (finally).");
        }
    }

    private HashMap<ReplyKind, String> whatAndHowReplyOn(String line) {
        HashMap<ReplyKind, String> reply = new HashMap<>(1);
        if (line.equals("/users")) {
            String userList = "";
            for (String user : usersStreams.keySet()) {
                userList = userList + user + "\n";
            }
            reply.put(ReplyKind.ULIST, userList + "> ");
            return reply;
        } else if (line.startsWith("@")) {
            //String toUser = line.substring(1, line.indexOf(" "));
            reply.put(ReplyKind.PRIV, "\n< " + currentThreadUser + ": " + line.substring(line.indexOf(" ") + 1) + "\n> ");
            return reply;
        } else {
            reply.put(ReplyKind.BCAST, "\n" + currentThreadUser + ": " + line + "\n> ");
            return reply;
        }
    }

    //@OnlyForTests
    protected HashMap<ReplyKind, String> whatAndHowReplyOnTest(String line) {
        currentThreadUser = "currentUsername";
        usersStreams.put("username1", new DataOutputStream(null));
        usersStreams.put("username2", new DataOutputStream(null));
        return whatAndHowReplyOn(line);
    }

    protected enum ReplyKind {BCAST, ULIST, PRIV}

    private class UserAlreadyExistsException extends RuntimeException {
        public UserAlreadyExistsException() {
            super("User with such nic already logged in");
        }
    }
}