package com.getjavajob.training.web05.samoylove.lesson13;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class ContactDAO {
    private static Connection connection;

    public ContactDAO() {
        connectToDB();
        System.out.println(getCount("contacts") + " contacts");
    }

    private void connectToDB() {
        try (InputStream fis = ContactDAO.class.getClassLoader().getResourceAsStream("dbconfig.properties")) {
            final Properties props = new Properties();
            props.load(fis);
            final String url = (System.getProperty("url") == null) ? props.getProperty("url") : System.getProperty("url");
            Class.forName("org.postgresql.Driver");
            System.out.println("url= " + url);
            connection = DriverManager.getConnection(url, props);
            connection.setAutoCommit(false);
        } catch (FileNotFoundException e) {
            System.err.println("Config file not found. " + e.getMessage());
        } catch (IOException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public ContactsList search(String name) {
        ContactsList resultContactsList = new ContactsList();
        String where = "";
        if (name != null) {
            where = "WHERE c.first_name = ? OR c.last_name = ? ";
        }
        final String query = "SELECT c.user_id, c.first_name, c.last_name, c.primary_phone_id, p.phone_id, p.phone, p.type " +
                "FROM contacts c " +
                "JOIN phones p " +
                "ON c.user_id = p.user_id " + where +
                "ORDER BY c.user_id, p.type";

        try (final PreparedStatement ps = connection.prepareStatement(query)) {
            if (name != null) {
                ps.setString(1, name);
                ps.setString(2, name);
            }
            try (final ResultSet resultSet = ps.executeQuery()) {
                int i = 0;
                PhonesSet phonesSet = null;
                Contact contact = null;
                Phone phone;
                while (resultSet.next()) {

                    int userId = resultSet.getInt("user_id");
                    if (i == 0 || (i > 0 && resultContactsList.get(i - 1).getId() != userId)) {
                        contact = new Contact(userId, resultSet.getString("first_name"), resultSet.getString("last_name"), resultSet.getInt("primary_phone_id"));
                        phonesSet = new PhonesSet();
                        i++;
                        resultContactsList.add(new Contact());
                    }
                    phone = new Phone(resultSet.getInt("phone_id"), resultSet.getString("phone"), resultSet.getString("type"));
                    phonesSet.addPhone(phone);
                    contact.setPhonesSet(phonesSet);
                    if (contact.getPrimPhone() == phone.getId()) {
                        phonesSet.setPrimary(phone.getId());
                    }
                    resultContactsList.set(i - 1, contact);
                }
            }
        } catch (SQLException e) {
            System.err.println("SQL Error in search() " + e.getMessage());
        }
        return resultContactsList;
    }

    public int add(String firstName, String lastName, String phone) {
        final String contactsQuery = "INSERT INTO contacts (first_name,last_name) VALUES (?,?)";
        final String phonesQuery = "INSERT INTO phones (user_id,phone) VALUES (?,?)";
        int userId = -1;

        try (final PreparedStatement ps = connection.prepareStatement(contactsQuery, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, firstName);
            ps.setString(2, lastName);
            ps.executeUpdate();
            final ResultSet generatedKeys = ps.getGeneratedKeys();
            generatedKeys.next();
            userId = generatedKeys.getInt("user_id");
        } catch (SQLException e) {
            rollback();
            System.err.println("SQL Error in add(), contactsQuery " + e.getMessage());
        }

        try (final PreparedStatement ps = connection.prepareStatement(phonesQuery, Statement.RETURN_GENERATED_KEYS)) {
            ps.setInt(1, userId);
            ps.setString(2, phone);
            ps.executeUpdate();
            final ResultSet generatedKeys = ps.getGeneratedKeys();
            generatedKeys.next();
            final int phoneID = generatedKeys.getInt("phone_id");
            connection.commit();
            setPrimary(userId, phoneID);
            System.out.println("Contact added. His ID: " + userId);
        } catch (SQLException e) {
            rollback();
            System.err.println("SQL Error in add(), phonesQuery " + e.getMessage());
        }
        return userId;
    }

    public int addPhone(boolean primary, int userId, String phone, String type) {
        final String query = "INSERT INTO phones (user_id,phone,type) VALUES (?,?,?)";
        int phoneID = -1;
        try (final PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            ps.setInt(1, userId);
            ps.setString(2, phone);
            ps.setString(3, type);
            ps.executeUpdate();
            final ResultSet generatedKeys = ps.getGeneratedKeys();
            generatedKeys.next();
            phoneID = generatedKeys.getInt("phone_id");
            connection.commit();
            if (primary) {
                setPrimary(userId, phoneID);
            }
        } catch (SQLException e) {
            rollback();
            System.err.println("SQL Error in addPhone() " + e.getMessage());
        }
        return phoneID;
    }

    private void rollback() {
        try {
            connection.rollback();
        } catch (SQLException e) {
            System.err.println("SQL Error in rollback()" + e.getMessage());
        }
    }

    protected int getCount(String what) {
        final String query = "SELECT COUNT(*) FROM " + what;
        int count = 0;
        try (final Statement statement = connection.createStatement()) {
            try (final ResultSet resultSet = statement.executeQuery(query)) {
                resultSet.next();
                count = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            System.err.println("SQL Error in getContactsCount()" + e.getMessage());
        }
        return count;
    }

    public void delete(int userId) {
        final String query = "DELETE FROM contacts WHERE user_id=?;";
        try (final PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, userId);
            ps.execute();
            connection.commit();
        } catch (SQLException e) {
            rollback();
            System.err.println("SQL Error in delete() " + e.getMessage());
        }
    }

    public void setPrimary(int userId, int phoneId) {
        final String query = "UPDATE contacts SET primary_phone_id=? WHERE user_id=?";
        try (final PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, phoneId);
            ps.setInt(2, userId);
            ps.execute();
            connection.commit();
        } catch (SQLException e) {
            rollback();
            System.err.println("SQL Error in setPrimary() " + e.getMessage());
        }
    }

    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}