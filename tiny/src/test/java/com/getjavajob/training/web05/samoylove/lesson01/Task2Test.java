package com.getjavajob.training.web05.samoylove.lesson01;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class Task2Test {

    @Test
    public void testWordsAfter() {
        String str = "Given text. Output all words starting with substring";
        List<String> rightResultList = Arrays.asList("all", "words", "starting", "with", "substring");

        List<String> wordsList = Task2.wordsAfter("Output", str);

        Assert.assertEquals(rightResultList, wordsList);
    }
}