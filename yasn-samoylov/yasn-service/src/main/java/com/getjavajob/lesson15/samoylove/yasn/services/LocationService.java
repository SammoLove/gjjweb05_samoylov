package com.getjavajob.lesson15.samoylove.yasn.services;

import com.getjavajob.lesson15.samoylove.yasn.entity.City;
import com.getjavajob.lesson15.samoylove.yasn.entity.Country;

import java.util.List;
import java.util.Set;

public interface LocationService {
	City readCity(int id);

	Country readCountry(int id);

	Set<City> getCitiesByCountryId(int id);

	List<Country> returnAllCountries();
}