package com.getjavajob.lesson15.samoylove.yasn.web;

import com.getjavajob.lesson15.samoylove.yasn.entity.City;
import com.getjavajob.lesson15.samoylove.yasn.entity.Country;
import com.getjavajob.lesson15.samoylove.yasn.entity.User;
import com.getjavajob.lesson15.samoylove.yasn.repository.SearchParams;
import com.getjavajob.lesson15.samoylove.yasn.services.LocationService;
import com.getjavajob.lesson15.samoylove.yasn.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/search")
public class SearchController {
	@Autowired
	UserService userService;
	@Autowired
	LocationService locationService;

	@RequestMapping(path = "/ajax", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public
	@ResponseBody
	List<UserForSearch> ajaxSearch(@RequestParam(value = "type", required = false) String type,
								   @RequestParam(value = "partName", required = false) String partName,
								   @RequestParam(value = "gender", required = false) String gender,
								   @RequestParam(value = "minAge", required = false) String minAge,
								   @RequestParam(value = "maxAge", required = false) String maxAge,
								   @RequestParam(value = "bCity", required = false) String bCity,
								   @RequestParam(value = "cCity", required = false) String cCity,
								   @RequestParam(value = "mybс", required = false) String mybс,
								   @RequestParam(value = "submit", required = false) String submit,
								   HttpServletRequest req) throws ServletException, IOException {

		List<User> foundedUsers = null;
		List<UserForSearch> searchResults = null;
		if (submit != null) {
			if (req.getParameterMap().size() > 0) {
				// todo avoid reqest
				final Map<String, String[]> parameterMap = req.getParameterMap();

				final SearchParams searchParams = new SearchParams(parameterMap);
				foundedUsers = userService.search(searchParams);

				//converting ro proper user info format (without password, etc.)
				searchResults = new ArrayList<>(foundedUsers.size());
				for (User user : foundedUsers) {
					UserForSearch userForSearch = new UserForSearch(user, locationService);
					searchResults.add(userForSearch);
				}
			}
		}
		return searchResults;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView search(@RequestParam(value = "type", required = false) String type,
							   @RequestParam(value = "partName", required = false) String partName,
							   @RequestParam(value = "gender", required = false) String gender,
							   @RequestParam(value = "minAge", required = false) String minAge,
							   @RequestParam(value = "maxAge", required = false) String maxAge,
							   @RequestParam(value = "bCity", required = false) String bCity,
							   @RequestParam(value = "cCity", required = false) String cCity,
							   @RequestParam(value = "mybс", required = false) String mybс,
							   @RequestParam(value = "submit", required = false) String submit,
							   HttpServletRequest req) throws ServletException, IOException {
		Map<String, Object> model = new HashMap<>();
		if (submit != null) {
			if (req.getParameterMap().size() > 0) {
				final Map<String, String[]> parameterMap = req.getParameterMap();
				final SearchParams searchParams = new SearchParams(parameterMap);
				final List<User> searchResults = userService.search(searchParams);
				model.put("searchResults", searchResults);
			}
			model.put("showResults", Boolean.TRUE);
		}
		loadLocationCatalog(model);
		return new ModelAndView("search", model);
	}

	private Map<String, Object> loadLocationCatalog(Map<String, Object> model) {
		final Iterable<Country> allCountries = locationService.returnAllCountries();

		Map<Integer, Country> allCountriesMap = new HashMap<>();
		Map<Integer, City> allCitiesMap = new HashMap<>();
		for (Country country : allCountries) {
			allCountriesMap.put(country.getId(), country);
			final Iterable<City> citiesOfCountry = locationService.getCitiesByCountryId(country.getId());
			for (City city : citiesOfCountry) {
				allCitiesMap.put(city.getId(), city);
			}
		}
		model.put("allCountries", allCountries);
		model.put("allCitiesMap", allCitiesMap);
		model.put("allCountriesMap", allCountriesMap);
		return model;
	}
}
