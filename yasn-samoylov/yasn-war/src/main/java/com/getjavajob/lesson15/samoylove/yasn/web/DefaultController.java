package com.getjavajob.lesson15.samoylove.yasn.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("*")
public class DefaultController {

    @RequestMapping(value = {"/{url}"}, method = RequestMethod.GET)
    public String redirectToWall(Model model, @PathVariable("url") String url) {
        model.addAttribute("notFoundedPage", url);
        return "404";
    }
}