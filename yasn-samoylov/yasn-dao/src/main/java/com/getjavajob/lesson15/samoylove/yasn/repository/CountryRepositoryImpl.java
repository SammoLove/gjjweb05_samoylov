package com.getjavajob.lesson15.samoylove.yasn.repository;

import com.getjavajob.lesson15.samoylove.yasn.entity.Country;
import org.springframework.stereotype.Repository;

@Repository
public class CountryRepositoryImpl extends AbstractCommonRepository<Country> implements CountryRepository {
    @Override
    protected Class<Country> getEntityClass() {
        return Country.class;
    }
}