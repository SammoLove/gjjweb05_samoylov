package com.getjavajob.lesson15.samoylove.yasn.repository;

import com.getjavajob.lesson15.samoylove.yasn.entity.TO;

import java.util.List;

public interface CommonDBOperations<T extends TO> {
    List<T> returnAll();

    Integer count();

    //boolean exist(int id);

    //Iterable<T> searchByCriteria(Criteria criteria);
}
