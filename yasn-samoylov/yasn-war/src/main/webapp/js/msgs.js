$(document).ready(function () {
    $('.interlocutor').click(function() {
	    var dialogId = $(this)[0].id;
	    var dialogTab =  $('#dialog'+dialogId)[0];
	    if (dialogTab == undefined) {
		    var s1 = $(this).text().trim();
		    var s2 = $(this)[0].innerText.trim();
		    var name = s1.substr(0, s1.length-s2.length);
		    createNewTab(dialogId, name);
	    } else {
		    activateTab(dialogId);
	    }
    });
});

function activateTab(dialogId) {
	var tabs = $("#tabs");
	tabs.find(".active".active).removeClass("active");
	tabs.find("#dialog" + dialogId).addClass("active");

	$(".tab-content .active").removeClass("active");
	$(".tab-content #dialog"+dialogId).addClass("active");
}

function getCorrespondence(dialogId, pageNumber){
	console.log('launched getCorrespondence('+dialogId+', '+pageNumber+')');
	var dialogBlock = "";
	var read = [];
	console.log(1);
	try {
		console.log("2-request");
		$.ajax({
			async: false,
			type: "GET",
			url: '/msgs/getCorrespondence' + dialogId + '-' + pageNumber,
			success: function (data){
				console.log("2-start (sync)");
				//var messages = JSON.parseJSON(data);
				for (var i = 0; i < data.length; i++) {
					dialogBlock += "<div><span>" + data[i].time + " " + data[i].sender.name + "</span><br>";
					dialogBlock += "<span>" + data[i].message + "</span></div>";
					if (data[i].readed == false) {
						read.push(data[i].id)
					}
				}
				dialogBlock += "<div><textarea class='form-control' rows='3'></textarea></div>";
			}
		});
	} catch(err) {
		console.log('Connection error while reading dialog!');
	}
	console.log(3);
	console.log('read:'+read+';');
	if (read.length > 0) {
		try {
			$.post('/msgs/markAsRead' + read);
		} catch(err) {
			console.log('Error. Was not marked read messages!')
		}
	}
	console.log('dialogBlock:'+dialogBlock+';');
	return dialogBlock;
}

function createNewTab(dialogId, dialogName) {
	//var dialogName = "Имя Фамилия (заменить)";
	//var dialogId = dialog23; // узнать автоматически
	var pageNumber = 1; // инкрементить при загрузке переписки

	//создаём вкладку (li, в него вложено a)
	//присоединяем к ul id="tabs"
	var newLiTab = $(document.createElement('li'))
			.attr('id', 'dialog'+dialogId)
			.appendTo($('#tabs'))
			.click(function() {
				activateTab(dialogId);
			});
	var newA = $(document.createElement('a'))
			.attr('href', '#dialog'+dialogId)
			.attr('data-toggle', 'tab')
			.addClass('tab-pane fade in active')
			.append(dialogName)
			.appendTo(newLiTab);

	// Добавляем только что созданный div на страницу

	var newDivTabContent = $(document.createElement('div'))
			.addClass('tab-pane')
			.attr('id', 'dialog'+dialogId)
			.appendTo($('#dialogs'));

	//создаём содержимое вкладки div class="tab-pane" id="dialog23"
	newDivTabContent.append(getCorrespondence(dialogId, pageNumber));
	activateTab(dialogId);
}