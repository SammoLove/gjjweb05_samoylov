package com.getjavajob.training.web05.samoylove.lesson01;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Task8Test {

    @Test
    public void testGroupBy() {
        final List<Integer> set = Arrays.asList(0, 1, 2, 3, 4, 5, 6);

        Function<Integer, Boolean> isEven = new Function<Integer, Boolean>() {
            @Override
            public Boolean apply(Integer argument) {
                return argument % 2 == 0;
            }
        };
        Map<Boolean, List<Integer>> rightIsEven = new LinkedHashMap<>();
        rightIsEven.put(Boolean.FALSE, Arrays.asList(1, 3, 5));
        rightIsEven.put(Boolean.TRUE, Arrays.asList(0, 2, 4, 6));

        Function<Integer, Boolean> isSimple = new Function<Integer, Boolean>() {
            @Override
            public Boolean apply(Integer argument) {
                for (int i = 2; i < argument / 2 + 1; i++) {
                    if (0 == argument % i) return false;
                }
                return true;
            }
        };
        Map<Boolean, List<Integer>> rightIsSimple = new LinkedHashMap<>();
        rightIsSimple.put(Boolean.FALSE, Arrays.asList(4, 6));
        rightIsSimple.put(Boolean.TRUE, Arrays.asList(0, 1, 2, 3, 5));

        Map<Boolean, List<Integer>> resultMapEvens = Task8.groupBy(set, isEven);
        Map<Boolean, List<Integer>> resultMapSimples = Task8.groupBy(set, isSimple);

        Assert.assertEquals(rightIsEven, resultMapEvens);
        Assert.assertEquals(rightIsSimple, resultMapSimples);
    }
}