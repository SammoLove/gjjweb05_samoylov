package com.getjavajob.lesson15.samoylove.yasn.services;

import com.getjavajob.lesson15.samoylove.yasn.entity.City;
import com.getjavajob.lesson15.samoylove.yasn.entity.Country;
import com.getjavajob.lesson15.samoylove.yasn.repository.CityRepository;
import com.getjavajob.lesson15.samoylove.yasn.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
public class LocationServiceImpl implements LocationService {
	private final CityRepository cityRepository;
	private final CountryRepository countryRepository;

	@Autowired
	public LocationServiceImpl(CityRepository cityRepository, CountryRepository countryRepository) {
		this.cityRepository = cityRepository;
		this.countryRepository = countryRepository;
	}

	@Override
	@Transactional
	public City readCity(int id) {
		return cityRepository.read(id);
	}

	@Override
	@Transactional
	public Country readCountry(int id) {
		return countryRepository.read(id);
	}

	@Override
	@Transactional
	public Set<City> getCitiesByCountryId(int id) {
		Country country = readCountry(id);
		return country.getCities();
	}

	@Override
	@Transactional
	public List<Country> returnAllCountries() {
		//Sort sort = new Sort(new Order(Direction.ASC,"")); //todo sorting
		return countryRepository.returnAll();
	}
}