package com.getjavajob.training.web05.samoylove.lesson09;

interface WordCounter {
    void countWords(String dirName);
}