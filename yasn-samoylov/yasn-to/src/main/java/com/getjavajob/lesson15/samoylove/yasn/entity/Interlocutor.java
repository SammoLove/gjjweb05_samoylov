package com.getjavajob.lesson15.samoylove.yasn.entity;

/**
 * Interlocutor - это собеседник в чате
 */
public class Interlocutor {
	private int id;
	private String name;
	private String nickname;
	private Long unreadMsgsCount;

	public Interlocutor() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Long getUnreadMsgsCount() {
		return unreadMsgsCount;
	}

	public void setUnreadMsgsCount(Long unreadMsgsCount) {
		this.unreadMsgsCount = unreadMsgsCount;
	}
}