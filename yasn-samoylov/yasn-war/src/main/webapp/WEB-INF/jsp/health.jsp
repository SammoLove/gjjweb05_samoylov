<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <style>
        html {
            background: linear-gradient(#fff, #ccc) fixed;
            width: 100%;
            height: 100%;
            display: table;
        }

        body {
            font: menu;
            display: table-cell;
            padding: 12px;
            vertical-align: middle;
        }

        #wrap {
            background-color: #fff;
            border-radius: 10px;
            border: 1px solid rgba(0, 0, 0, .25);
            box-shadow: 0 0 10px rgba(0, 0, 0, .25);
            margin: 0 auto;
            max-width: 56em;
            padding: 30px;
            position: relative;
        }

        h2 {
            font-size: 1em;
            margin-top: 0;
        }
    </style>
</head>
<body>
<%@ include file="/WEB-INF/jsp/header.jsp" %>
<div id="wrap">

    <h1>this is health.jsp, here will be many diagnostic information.</h1>

    <p>Users in database: ${userCount}  </p>

    <p>Is logged in? <%= session.getAttribute("loginInfo") %>
    </p>

    <p>Session getServletContext: <%= session.getServletContext() %>

    <p>Session id: <%= session.getId() %>

    <p>Session isNew: <%= session.isNew() %>

    <p>Session getAttributeNames: <%= session.getAttributeNames() %>

    <p>Request getRequestURI: <%= request.getRequestURI() %>

    <p>Request getQueryString: <%= request.getQueryString() %>

    <p>Request getContextPath: <%= request.getContextPath() %>

    <p>Request getMethod: <%= request.getMethod() %>

    <p>Request getCookies: <%= request.getCookies() %>

    <p>Request getAttributeNames: <%= request.getAttributeNames() %>

    <p>Request getAuthType: <%= request.getAuthType() %>

    <p>Request getHeaderNames: <%= request.getHeaderNames() %>
</div>
</body>
</html>