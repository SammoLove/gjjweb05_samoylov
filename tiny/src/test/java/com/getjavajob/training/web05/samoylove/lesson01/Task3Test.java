package com.getjavajob.training.web05.samoylove.lesson01;

import gnu.trove.list.array.TIntArrayList;
import org.junit.Assert;
import org.junit.Test;

public class Task3Test {

    @Test
    public void testCountEven() {
        TIntArrayList example = new TIntArrayList(new int[]{0, 1, 2, 3, 4, 5, 6});

        int evens = Task3.countEven(example);

        Assert.assertEquals(4, evens);
    }
}