package com.getjavajob.lesson15.samoylove.yasn.entity;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.sql.Time;

@StaticMetamodel(Message.class)
public class Message_ {
    public static volatile SingularAttribute<Message, Integer> id;
    public static volatile SingularAttribute<Message, Time> time;
    public static volatile SingularAttribute<Message, String> message;
    public static volatile SingularAttribute<Message, User> receiver;
    public static volatile SingularAttribute<Message, User> sender;
    public static volatile SingularAttribute<Message, Boolean> readed;
    //public static volatile SingularAttribute<Message, Avatar> files;
    public static volatile SingularAttribute<Message, Boolean> priv;
}