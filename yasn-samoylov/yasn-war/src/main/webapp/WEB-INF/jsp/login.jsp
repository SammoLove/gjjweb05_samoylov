<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Login</title>
	<%--<link href="<c:url value="/css/wall.css" />" rel="stylesheet">--%>
	<link href="<c:out value="${pageContext.request.contextPath}"/>/css/style.css" rel="stylesheet">
	<link href="<c:out value="${pageContext.request.contextPath}"/>/css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<div class="container" style="margin-top: 60px" id="page"> <!--width: 900px-->
	<div class="row">
		<%@ include file="/WEB-INF/jsp/leftcolumn.jsp" %>
		<div class="col-sm-9" id="page-content">

			<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
				<form role="form" action="/login" method="POST">
					<fieldset>
						<legend>Please Sign In</legend>
						<%--<h3>Please Sign In</h3>--%>
						<hr class="colorgraph">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
								<input type="email" name="email" id="email" class="form-control"
									   placeholder="Email Address" autofocus>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
								<input type="password" name="password" id="password" class="form-control"
									   placeholder="Password">
							</div>
						</div>
				<span class="button-checkbox">
					<button type="button" class="btn" data-color="info">Short session (15 min)</button>
                    <input type="checkbox" name="shortSession" id="shortSession" class="hidden">
					<%--<a href="" class="btn btn-link pull-right">Forgot Password?</a>--%>
				</span>
						<hr class="colorgraph">
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6">
								<input type="submit" class="btn btn-lg btn-success btn-block" value="Sign In">
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6">
								<a href="/register" class="btn btn-lg btn-primary btn-block">Register</a>
							</div>
						</div>
						<input type="hidden" name="returnUrl" class="login-input" value="<c:out value="${param.returnUrl}"/>">
					</fieldset>
				</form>
			</div>
			<%
				session.setAttribute("login", Boolean.TRUE);
				request.setAttribute("requestedPage", request.getRequestURI()); //или в сессию?
			%>
		</div>
		<!--end page-content-->
		<%@ include file="/WEB-INF/jsp/footer.jsp" %>
		<%--</div>--%>
		<script src="<c:out value="${pageContext.request.contextPath}"/>/js/login.js"></script>
</body>
</html>