<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Wall</title>
    <%--<link href="<c:url value="/css/wall.css" />" rel="stylesheet">--%>
    <link href="<c:out value="${pageContext.request.contextPath}"/>/css/style.css" rel="stylesheet">
    <link href="<c:out value="${pageContext.request.contextPath}"/>/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<div class="container" style="margin-top: 60px" id="page"> <!--width: 900px-->
    <div class="row">
        <%@ include file="/WEB-INF/jsp/leftcolumn.jsp" %>
        <div class="col-sm-9" id="page-content">
            <div class="alert alert-success alert-dismissable" id="system-message">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Done!</strong> You information was saved successfully.
            </div>
            <div class="row">
                <%--@elvariable id="wallUser" type="com.getjavajob.lesson15.samoylove.yasn.entity.User"--%>
                <div class="col-sm-5" id="ava">
                    <a href="/ava/${wallUser.id}?down=true" class="thumbnail">
                        <img class="img-rounded" data-src="holder.js/100%x180" alt="ava"
                             src="<c:out value="/ava/${wallUser.id}"/>"/>
                        <%--img/default-photo.jpg--%>
                    </a>
                </div>
                <div class="col-sm-7">
                    <h1 style="margin-top: 5px"><c:out value="${ wallUser.name }"/></h1>

                    <h2>Known as <c:out value="${ wallUser.nickname }"/></h2>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Information</h3>
                        </div>
                        <div class="panel-body" id="user-info">
                            <dl class="dl-horizontal">
                                <dt>ID:</dt>
                                <dd><c:out value="${ wallUser.id }"/></dd>
                                <dt>Name:</dt>
                                <dd><c:out value="${ wallUser.name }"/></dd>
                                <dt>Gender:</dt>
                                <dd><c:out value="${ wallUser.gender }"/></dd>
                                <dt>Birth:</dt>
                                <dd><c:out value="${ wallUser.birth }"/></dd>
                                <dt>Age:</dt>
                                <dd><c:out value="${ wallUser.age }"/></dd>
                                <%--@elvariable id="bornCity" type="com.getjavajob.lesson15.samoylove.yasn.entity.City"--%>
                                <dt>Born in:</dt>
                                <dd><c:out value="${bornCity.name}, ${bornCity.country.name}"/></dd>
                                <%--@elvariable id="liveCity" type="com.getjavajob.lesson15.samoylove.yasn.entity.City"--%>
                                <dt>Live in:</dt>
                                <dd><c:out value="${liveCity.name}, ${liveCity.country.name}"/></dd>
                            </dl>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-12">
                            <!--  START -->
                            <div class="sm-wrapper">
                                <div class="panel panel-default">

                                    <div class="panel-body">
                                        <!-- <a href="#" class="btn btn-info ">
											<i class="glyphicon glyphicon-pencil"></i> &nbsp; View Code
										</a>
										<a href="#" class="btn btn-success ">
											<i class="glyphicon glyphicon-off"></i> &nbsp; Clear History
										</a>
												   <a href="#" class="btn btn-warning ">
											<i class="glyphicon glyphicon-eye-open"></i> &nbsp; Opened
										</a> -->
                                        <!-- <hr /> -->
                                        <textarea class="form-control" placeholder="Write your message..."
                                                  rows="3"></textarea>
                                        <!-- <hr /> -->
                                        <div class="clearfix"></div>

                                        <a href="#" class="btn btn-success ">
                                            <i class="glyphicon glyphicon-share"></i> &nbsp; SEND MESSAGE
                                        </a>
                                        <div class="clearfix"></div>
                                        <hr>
                                        <ul class="media-list">

                                            <li class="media">
                                                <a href="#" class="pull-left">
                                                    <img src="assets/img/2.png" alt="" class="img-rounded">
                                                </a>
                                                <div class="media-body">

                                                    <p>This is my message to you. Lorem ipsum dolor sit amet,
                                                        consectetur adipiscing elit.
                                                    </p>
                                        <span class="text-muted">
                                            <small class="text-muted">Last message by <i> John Doe </i> on dt. 23rd
                                                November
                                            </small>
                                        </span>
                                                    <br/>
                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--  END -->
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!--end page-content-->
        <%@ include file="/WEB-INF/jsp/footer.jsp" %>
</body>
</html>