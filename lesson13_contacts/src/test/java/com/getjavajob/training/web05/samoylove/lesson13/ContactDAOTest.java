package com.getjavajob.training.web05.samoylove.lesson13;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ContactDAOTest {
    private static ContactDAO contactDAO;

    @Before
    public void init() {
        contactDAO = new ContactDAO();
    }

    @Test
    public void testSearch() {

    }

    @Test
    public void testAdd() {

    }

    @Test
    public void testAddPhone() {

    }

    @Test
    public void testGetContactsCount() {

    }

    @Test
    public void testDelete() {
        int contactsCountWas = contactDAO.getCount("contacts");
        int phonesCountWas = contactDAO.getCount("phones");
        contactDAO.delete(2);
        int contactsCountNow = contactDAO.getCount("contacts");
        int phonesCountNow = contactDAO.getCount("phones");
        Assert.assertEquals(contactsCountNow, contactsCountWas - 1);
        Assert.assertTrue(phonesCountWas > phonesCountNow);
    }

    @Test
    public void testSetPrimary() {

    }
}