package com.getjavajob.training.web05.samoylove.lesson01;

import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

public class Task6Test {

    @Test
    public void testIntersectionSets() {
        Set<Integer> set1 = Sets.newHashSet(1, 2, 3, 4);
        Set<Integer> set2 = Sets.newHashSet(3, 4, 5, 6);
        Set<Integer> rightIntersection = Sets.newHashSet(3, 4);

        Set result = Task6.intersectionSets(set1, set2);

        Assert.assertEquals(rightIntersection, result);
    }
}