package com.getjavajob.training.web05.samoylove.lesson01;

//import junit.framework.Assert;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class Task4Test {

    @Test
    public void testFindMinMaxDif() {
        Byte[] data1 = {6, 3, 5, 2};
        List<Byte> a1 = Arrays.asList(data1); //takes any itarable type
        Long[] data2 = {8L, 5L, 5L, 1L};
        HashSet<Long> a2 = new HashSet<>(Arrays.asList(data2));

        int diff1 = Task4.findMinMaxDif(a1);
        int diff2 = Task4.findMinMaxDif(a2);

        Assert.assertEquals(4, diff1); //6-2
        Assert.assertEquals(7, diff2); //8-1
    }
}