package com.getjavajob.training.web05.samoylove.lesson01;

import java.util.*;

interface Function<A, R> {
    R apply(A arg);
}

public class Task8 {
    public static <K, E> Map<K, List<E>> groupBy(Collection<E> collection, Function<E, K> mapper) {
        Map<K, List<E>> output = new HashMap<>();
        for (E element : collection) {
            K category = mapper.apply(element);
            List<E> elementsList = output.get(category);
            if (elementsList == null) {
                elementsList = new LinkedList<>();
            }
            elementsList.add(element);
            output.put(category, elementsList);
        }
        return output;
    }
}