<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Messages</title>
    <%--<link href="<c:url value="/css/wall.css" />" rel="stylesheet">--%>
    <link href="<c:out value="${pageContext.request.contextPath}"/>/css/style.css" rel="stylesheet">
    <link href="<c:out value="${pageContext.request.contextPath}"/>/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<div class="container" style="margin-top: 60px" id="page"> <!--width: 900px-->
    <div class="row">
        <%@ include file="/WEB-INF/jsp/leftcolumn.jsp" %>
        <div class="col-sm-9" id="page-content">
            <ul class="nav nav-tabs" id="tabs">
                <li class="active"><a href="#allDialogs" data-toggle="tab">All dialogs</a></li>

                <%--вот здесь создавать новые вкладки--%>
            </ul>

            <div class="tab-content" id="dialogs">
                <div class="tab-pane active" id="allDialogs">
                    <%--@elvariable id="interlocutors" type="java.util.List"--%>
                    <%--@elvariable id="interlocutor" type="com.getjavajob.lesson15.samoylove.yasn.entity.Interlocutor"--%>
                    <div class="col-sm6 list-group">
                        <span class="list-group-item active">
                            <c:if test="${interlocutors.size() > 0}">
                                ${interlocutors.size()} dialogs
                            </c:if>
                            <c:if test="${interlocutors.size() == 0}">You don't have any dialog</c:if>
                        </span>

                        <c:forEach var="interlocutor" items="${interlocutors}" varStatus="ctr">
                            <a href="#" class="interlocutor list-group-item" id="<c:out value="${interlocutor.id}"/>">
                                <c:out value="${interlocutor.name}"/>
                                (<c:out value="${interlocutor.nickname}"/>)
								<span class="badge" id="unread-count">
									<c:out value="${interlocutor.unreadMsgsCount}"/>
								</span>
                            </a>
                        </c:forEach>
                    </div>
                </div>
                <%--вот здесь создавать содержимое новой вкладки, например--%>
            </div>
        </div>
        <!--end page-content-->
        <%@ include file="/WEB-INF/jsp/footer.jsp" %>
        <script src="<c:out value="${pageContext.request.contextPath}"/>/js/msgs.js"></script>
</body>
</html>