package com.getjavajob.training.web05.samoylove.lesson06;

import org.junit.Test;

import java.io.File;
import java.util.Collection;
import java.util.Map;

import static com.getjavajob.training.web05.samoylove.lesson06.Task2.*;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class Task2Test {
    File XMLFile = new File("src\\main\\java\\com\\getjavajob\\training\\web05\\samoylove\\lesson02\\Task2.xml");
    Product oneOf = new Product("Lexus CT", 1, 45000.0);

    @Test
    public void testLoadGoodsByDOM() {
        Map<String, Collection<Task2.Product>> priceList = loadGoodsByDOM(XMLFile);

        assertTrue(priceList.get("Comfort").contains(oneOf));
        assertSame(2, priceList.get("Economy").size());
        assertSame(2, priceList.keySet().size());
        assertSame(2, priceList.size());
    }

    @Test
    public void testLoadGoodsBySAX() {
        Map<String, Collection<Task2.Product>> priceList = loadGoodsBySAX(XMLFile);

        assertTrue(priceList.get("Comfort").contains(oneOf));
        assertSame(2, priceList.get("Economy").size());
        assertSame(2, priceList.keySet().size());
        assertSame(2, priceList.size());
    }

    @Test
    public void testLoadGoodsBySTAX() {
        Map<String, Collection<Task2.Product>> priceList = loadGoodsBySTAX(XMLFile);

        assertTrue(priceList.get("Comfort").contains(oneOf));
        assertSame(2, priceList.get("Economy").size());
        assertSame(2, priceList.keySet().size());
        assertSame(2, priceList.size());
    }
}