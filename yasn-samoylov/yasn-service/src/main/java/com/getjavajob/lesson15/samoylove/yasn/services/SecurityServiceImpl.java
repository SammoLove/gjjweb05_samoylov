package com.getjavajob.lesson15.samoylove.yasn.services;

import com.getjavajob.lesson15.samoylove.yasn.entity.SecuritySetting;
import com.getjavajob.lesson15.samoylove.yasn.repository.SecurityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SecurityServiceImpl implements SecurityService {
	private final SecurityRepository securityRepository;

	@Autowired
	public SecurityServiceImpl(SecurityRepository securityRepository) {
		this.securityRepository = securityRepository;
	}

	@Transactional
	public SecuritySetting create(SecuritySetting securitySetting) {
		return securityRepository.create(securitySetting);
	}

	@Transactional
	public SecuritySetting read(int id) {
		return securityRepository.read(id);
	}

	@Transactional
	public void update(SecuritySetting securitySetting) {
		securityRepository.update(securitySetting);
	}
}