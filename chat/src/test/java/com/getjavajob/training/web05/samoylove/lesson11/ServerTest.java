package com.getjavajob.training.web05.samoylove.lesson11;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

public class ServerTest {

    @Test
    public void testWhatAndHowReplyOn() {
        Server server = new Server();
        String line;
        HashMap<Server.ReplyKind, String> reply;
        HashMap<Server.ReplyKind, String> right = new HashMap<>();

        line = "/users";
        reply = server.whatAndHowReplyOnTest(line);
        right.put(Server.ReplyKind.ULIST, "username2\nusername1\n> ");
        Assert.assertEquals(right, reply);
        right.clear();

        line = "@username2 Private message";
        reply = server.whatAndHowReplyOnTest(line);
        right.put(Server.ReplyKind.PRIV, "\n< currentUsername: Private message\n> ");
        Assert.assertEquals(right, reply);
        right.clear();

        line = "Message to all";
        reply = server.whatAndHowReplyOnTest(line);
        right.put(Server.ReplyKind.BCAST, "\ncurrentUsername: Message to all\n> ");
        Assert.assertEquals(right, reply);
    }
}