package com.getjavajob.lesson15.samoylove.yasn.web;

import javax.servlet.*;
import java.io.IOException;

public class SkipStaticFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        req.setAttribute("isStatic", Boolean.TRUE);
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }
}
