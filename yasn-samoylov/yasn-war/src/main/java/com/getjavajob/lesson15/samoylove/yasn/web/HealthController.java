package com.getjavajob.lesson15.samoylove.yasn.web;

import com.getjavajob.lesson15.samoylove.yasn.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HealthController {
    @Autowired
    UserService userService;

    @RequestMapping(path = "/health", method = RequestMethod.GET)
    public ModelAndView getHealthPage() {
        Integer userCount = userService.count();
        return new ModelAndView("health", "userCount", userCount);
    }
}