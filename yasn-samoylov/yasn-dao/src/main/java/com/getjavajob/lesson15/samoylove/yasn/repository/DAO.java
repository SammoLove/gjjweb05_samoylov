package com.getjavajob.lesson15.samoylove.yasn.repository;

import com.getjavajob.lesson15.samoylove.yasn.entity.TO;

public interface DAO<T extends TO> {

    T create(T newObject);

    T read(int id);

    void update(T object);

    void delete(T object);

    void deleteById(int id);
}