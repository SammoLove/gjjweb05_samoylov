package com.getjavajob.training.web05.samoylove.lesson13;

import java.util.HashMap;
import java.util.TreeSet;

public class PhonesSet {
    private TreeSet<Phone> phoneSet;
    private int primary;
    private HashMap<String, Integer> typeCount;

    public PhonesSet() {
        phoneSet = new TreeSet<>(new SortPhonesByType());
        typeCount = new HashMap<>();
    }

    public void setPrimary(int primary) {
        this.primary = primary;
    }

    public void addPhone(Phone phone) {
        phoneSet.add(phone);
        final Integer oldVal = typeCount.get(phone.getPhoneType()) == null ? 0 : typeCount.get(phone.getPhoneType());
        typeCount.put(phone.getPhoneType(), oldVal + 1);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();

        final String newLine = System.getProperty("line.separator");
        String currentType = "";

        for (Phone phone : phoneSet) {
            if (!currentType.equals(phone.getPhoneType())) {
                currentType = phone.getPhoneType();
                sb.append("  ").append(currentType).append(" [").append(typeCount.get(currentType)).append(']').append(newLine);
            }
            if (phone.getId() == primary) {
                sb.append("   *");
            } else {
                sb.append("    ");
            }
            sb.append(phone).append(newLine);
        }
        return sb.toString();
    }
}