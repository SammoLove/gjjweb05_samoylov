package com.getjavajob.lesson15.samoylove.yasn.repository;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class SearchParams {
    private String partName;
    private String nickname;
    private Boolean gender;
    private Date minAge;
    private Date maxAge;
    private Integer birthCountry;
    private Integer birthCity;
    private Integer currCountry;
    private Integer currCity;

    public SearchParams() {
    }

    public SearchParams(Map<String, String[]> parameterMap) {
        if (parameterMap == null) {
            return;
        }
        if (parameterMap.get("partName") != null && !parameterMap.get("partName")[0].isEmpty()) {
            partName = parameterMap.get("partName")[0];
        }
        if (parameterMap.get("gender") != null && !parameterMap.get("gender")[0].isEmpty()) {
            gender = Boolean.valueOf(parameterMap.get("gender")[0]);
        }
        if (parameterMap.get("minAge") != null && !parameterMap.get("minAge")[0].isEmpty()) {
            Integer age = Integer.valueOf(parameterMap.get("minAge")[0]); //20
            Calendar dob = Calendar.getInstance(); //07 10 2015
            dob.set(Calendar.YEAR, dob.get(Calendar.YEAR) - age); //07 10 1995
            minAge = dob.getTime();
        }
        if (parameterMap.get("maxAge") != null && !parameterMap.get("maxAge")[0].isEmpty()) {
            Integer age = Integer.valueOf(parameterMap.get("maxAge")[0]); //30
            Calendar dob = Calendar.getInstance(); //07 10 2015
            dob.set(Calendar.YEAR, dob.get(Calendar.YEAR) - age); //07 10 1985
            maxAge = dob.getTime();
        }
        if (parameterMap.get("birthCountry") != null && !parameterMap.get("birthCountry")[0].isEmpty()) {
            birthCountry = Integer.valueOf(parameterMap.get("birthCountry")[0]);
        }
        if (parameterMap.get("birthCity") != null && !parameterMap.get("birthCity")[0].isEmpty()) {
            birthCity = Integer.valueOf(parameterMap.get("birthCity")[0]);
        }
        if (parameterMap.get("currCountry") != null && !parameterMap.get("currCountry")[0].isEmpty()) {
            currCountry = Integer.valueOf(parameterMap.get("currCountry")[0]);
        }
        if (parameterMap.get("currCity") != null && !parameterMap.get("currCity")[0].isEmpty()) {
            currCity = Integer.valueOf(parameterMap.get("currCity")[0]);
        }
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public Date getMinAge() {
        return minAge;
    }

    public void setMinAge(Date minAge) {
        this.minAge = minAge;
    }

    public Date getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(Date maxAge) {
        this.maxAge = maxAge;
    }

    public Integer getBirthCountry() {
        return birthCountry;
    }

    public void setBirthCountry(Integer birthCountry) {
        this.birthCountry = birthCountry;
    }

    public Integer getBirthCity() {
        return birthCity;
    }

    public void setBirthCity(Integer birthCity) {
        this.birthCity = birthCity;
    }

    public Integer getCurrCountry() {
        return currCountry;
    }

    public void setCurrCountry(Integer currCountry) {
        this.currCountry = currCountry;
    }

    public Integer getCurrCity() {
        return currCity;
    }

    public void setCurrCity(Integer currCity) {
        this.currCity = currCity;
    }
}
