package com.getjavajob.training.web05.samoylove.lesson04;

import java.util.Arrays;
import java.util.Collection;

public class Task1 {
    static Collection<Class<?>> findAllTestClasses() {
        //was
        //return Arrays.asList(Task1.class);
        //there was mistake required Collection<Class<?>>, found List<Class<Task1>>
        //due the raw type returned by asList
        //right working version will be follow:
        return Arrays.<Class<?>>asList(Task1.class);
        //or need to use several asList arguments
        //return Arrays.asList(Task1.class, Task2.class);
    }
}