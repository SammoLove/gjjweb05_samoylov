package com.getjavajob.training.web05.samoylove.lesson11;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

class Client {
    private final int port;
    private final String host;
    private final String user;

    public Client(int p, String h, String u) {
        port = p;
        host = h;
        user = u;
        work();
    }

    public void work() {
        System.out.println("Trying to connect with " + host + " and port " + port + "...");
        Socket clientSocket;
        try {
            InetAddress ipAddress = InetAddress.getByName(host);
            clientSocket = new Socket(ipAddress, port);
            System.out.println("Server found. " + clientSocket.toString());
        } catch (IOException e) {
            System.err.println("Sever is not found!");
            return;
        }

        try (final DataInputStream dataInputStream = new DataInputStream(clientSocket.getInputStream());
             final DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream())) {

            dataOutputStream.writeUTF("!" + user);
            dataOutputStream.flush();


            final Thread userInputThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
                    while (true) {
                        try {
                            String userInput = keyboard.readLine();
                            if (userInput.equals("/logout")) {
                                dataInputStream.close(); //braking Stream to exit.
                            }
                            dataOutputStream.writeUTF(userInput);
                            dataOutputStream.flush();
                        } catch (IOException e) {
                            System.out.println("You're logged out.");
                        }
                    }
                }
            }, "user input");
            userInputThread.setDaemon(true);
            userInputThread.start();

            while (true) {
                System.out.print(dataInputStream.readUTF());
            }
        } catch (IOException e) {
            System.out.println("Connection with server closed.");
        }
    }
}