package com.getjavajob.training.web05.samoylove.lesson01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Task2 {
    public static List<String> wordsAfter(String text, String input) {
        List<String> output = new ArrayList<>();
        int from = input.indexOf(text) + text.length() + 1;
        output.addAll(Arrays.asList(input.substring(from).split(" ")));
        return output;
    }
}