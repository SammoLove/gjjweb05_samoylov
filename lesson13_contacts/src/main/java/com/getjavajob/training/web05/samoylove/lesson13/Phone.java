package com.getjavajob.training.web05.samoylove.lesson13;

import java.util.Objects;

public class Phone {
    private int id;
    private String phone;
    private String phoneType;

    public Phone(int id, String phone, String phoneType) {
        this.id = id;
        this.phone = phone;
        this.phoneType = phoneType;
    }

    public int getId() {
        return id;
    }

    public String getPhoneType() {
        return phoneType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Phone)) return false;
        Phone phone = (Phone) o;
        return Objects.equals(id, phone.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, phone, phoneType);
    }

    @Override
    public String toString() {
        return "(" + id + ") " + phone;
    }
}
