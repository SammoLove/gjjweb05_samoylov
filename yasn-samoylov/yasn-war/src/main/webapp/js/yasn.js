$(document).ready(function () {
	//if logged in
	//showUnreadCount();

	$('#unread-count').click(function (){
		showUnreadCount();
	});
});

function showUnreadCount(){
	$.get('/msgs/unreadCount', function (data){
		var uc = $('#unread-count');
		uc.empty();
		uc.append(data).append('&nbsp;<span class="glyphicon glyphicon-refresh" id="msg-count-refresh"></span>');
	})
}