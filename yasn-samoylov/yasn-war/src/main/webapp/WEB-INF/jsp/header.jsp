<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!--header to include start-->

<div class="navbar navbar-inverse navbar-fixed-top" id="header-bar"> <!--navbar-static-top-->
    <div class="container"><!-- style="width: 900px"-->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#responsive-menu">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand"><i class="glyphicon glyphicon-star-empty"></i>YASN</a>
        </div>

        <div class="collapse navbar-collapse" id="responsive-menu">
            <div class="row">
                <ul class="nav navbar-nav">
                    <li><a href="#"><i class="glyphicon glyphicon-"></i>Wall</a></li>
                </ul>


                <div>
                <form class="input-group navbar-form navbar-right" role="form"
                      action="<c:out value="${pageContext.request.contextPath}"/>/search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search by name/nick"
                               name="partName" id="qSearch"/>
						<span class="input-group-btn">
							<button class="btn btn-default" type="submit">
                                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>&nbsp;
                            </button>
						</span>
                    </div>
                    <%--<input type="hidden" name="submit" value="q">--%>
                </form>
                </div>

            </div>
        </div>
    </div>
</div>
<%--header to include end--%>