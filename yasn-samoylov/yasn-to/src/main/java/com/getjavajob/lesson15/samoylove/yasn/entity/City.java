package com.getjavajob.lesson15.samoylove.yasn.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class City extends TO {
	@Id
	@SequenceGenerator(name = "city_id_seq", sequenceName = "city_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "city_id_seq")
	private int id;

	@NotNull
	private String name;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL)
	@JoinColumn(name = "country_id")
	private Country country;

	public City() {
	}

	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
}