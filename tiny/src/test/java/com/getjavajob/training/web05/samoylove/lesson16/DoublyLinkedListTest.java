package com.getjavajob.training.web05.samoylove.lesson16;

import org.junit.Assert;
import org.junit.Test;

public class DoublyLinkedListTest {

    @Test
    public void testAddAndGet() {
        DoublyLinkedList<Integer> myList = new DoublyLinkedList<>();
        myList.add(1);
        myList.add(3);
        myList.add(2);
        Assert.assertEquals(new Integer(1), myList.get(0));
        Assert.assertEquals(new Integer(3), myList.get(1));
        Assert.assertEquals(new Integer(2), myList.get(2));
        System.out.println("testAddAndGet:");
        System.out.println(myList);
    }

    @Test
    public void sort() {
        DoublyLinkedList<Integer> myList = new DoublyLinkedList<>();
        myList.add(2);
        myList.add(3);
        myList.add(1);
        ListSorter<Integer> listSorter = new ListSorter<>();
        myList = listSorter.mergeSort(myList);
        Assert.assertEquals(new Integer(1), myList.get(0));
        Assert.assertEquals(new Integer(2), myList.get(1));
        Assert.assertEquals(new Integer(3), myList.get(2));
        System.out.println("sort:");
        System.out.println(myList);
    }
}