package com.getjavajob.training.web05.samoylove.lesson13;

import java.util.LinkedList;

public class ContactsList {
    private LinkedList<Contact> contactsList;

    public ContactsList() {
        contactsList = new LinkedList<>();
    }

    public boolean add(Contact contact) {
        return contactsList.add(contact);
    }

    public Contact get(int index) {
        return contactsList.get(index);
    }

    public Contact set(int index, Contact contact) {
        return contactsList.set(index, contact);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        for (Contact contact : contactsList) {
            sb.append(contact);
        }
        return sb.toString();
    }
}