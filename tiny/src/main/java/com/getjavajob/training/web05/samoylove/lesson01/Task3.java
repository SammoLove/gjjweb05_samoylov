package com.getjavajob.training.web05.samoylove.lesson01;

// Trying to work with the Trove - an external library

import gnu.trove.iterator.TIntIterator;
import gnu.trove.list.array.TIntArrayList;

public class Task3 {
    public static int countEven(TIntArrayList input) {
        int counter = 0;
        TIntIterator iter = input.iterator();
        while (iter.hasNext()) {
            int element = iter.next();
            if (element % 2 == 0) {
                counter++;
            }
        }
        return counter;
    }
}