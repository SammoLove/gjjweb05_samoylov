package com.getjavajob.training.web05.samoylove.lesson03;

import org.junit.Assert;
import org.junit.Test;

public class Task2Test {
    @Test
    public void testOrganization() {
        Employee employee1 = new Employee("Вася", "ул. Пушкина", "охранник");
        Employee employee2 = new Employee("Петя", "ул. Ленина", "продавец");
        Manager manager1 = new Manager("Сергей", "пр. Победы");
        Assert.assertTrue(manager1.addEmployee(employee1));
        Assert.assertTrue(manager1.addEmployee(employee2));

        Employee employee1Copy = null;
        Manager manager1Copy = null;
        try {
            employee1Copy = employee1.clone();
            manager1Copy = manager1.clone();
        } catch (CloneNotSupportedException e) {
            Assert.fail("CloneNotSupportedException");
        }

        Assert.assertEquals(manager1, manager1Copy);
        Assert.assertEquals(employee1, employee1Copy);

        employee1Copy.setName("New Вася copy");
        employee1.setName("New Вася");
        Assert.assertTrue(manager1Copy.addEmployee(employee1Copy));
        Assert.assertFalse(manager1Copy.removeEmployee(employee1)); //name was changed, should not find it
        Assert.assertTrue(manager1Copy.removeEmployee(employee2));
    }
}