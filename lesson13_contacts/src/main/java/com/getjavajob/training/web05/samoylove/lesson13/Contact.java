package com.getjavajob.training.web05.samoylove.lesson13;

public class Contact {
    private int id;
    private String firstName;
    private String lastName;
    private int primPhone;
    private PhonesSet phonesSet;

    public Contact() {
    }

    public Contact(int id, String firstName, String lastName, int primPhone) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.primPhone = primPhone;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public PhonesSet getPhonesSet() {
        return phonesSet;
    }

    public void setPhonesSet(PhonesSet phonesSet) {
        this.phonesSet = phonesSet;
    }

    public int getId() {
        return id;
    }

    public int getPrimPhone() {
        return primPhone;
    }

    @Override
    public String toString() {
        final String template = "(%s) %s %s\n%s";
        return String.format(template, id, firstName, lastName, phonesSet);
    }
}