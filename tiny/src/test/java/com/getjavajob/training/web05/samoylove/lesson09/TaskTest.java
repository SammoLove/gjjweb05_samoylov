package com.getjavajob.training.web05.samoylove.lesson09;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class TaskTest {
    @Test
    public void testCountWords() {
        //String dirName = "src\\test\\resources\\texts\\";
        //Task t = new Task();
        //t.countWords(dirName); //real writing to words.txt. It is not necessary for automatic testing

        String example = "An object that maps keys to values. A map cannot contain duplicate keys; each key can map to at most one value. Set of key-value mappings";
        Task t = new Task();
        Map<String, Integer> words;

        words = t.handleLine(example);
        Assert.assertEquals(23, words.size());
        Assert.assertEquals(1, (int) words.get("key-value"));
        Assert.assertEquals(2, (int) words.get("map"));
    }
}