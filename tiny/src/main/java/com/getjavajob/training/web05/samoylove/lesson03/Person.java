package com.getjavajob.training.web05.samoylove.lesson03;

public class Person implements Cloneable {
    private String name;
    private String homeAddress;

    public Person(String name, String homeAddress) {
        this.name = name;
        this.homeAddress = homeAddress;
    }

    @Override
    public Person clone() throws CloneNotSupportedException {
        Person copy = (Person) super.clone();
        copy.name = new String(this.name); //or new String(this.name) ?
        copy.homeAddress = new String(this.homeAddress);
        return copy;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
