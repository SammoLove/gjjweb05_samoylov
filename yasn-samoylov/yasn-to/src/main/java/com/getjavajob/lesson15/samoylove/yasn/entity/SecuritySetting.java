package com.getjavajob.lesson15.samoylove.yasn.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "private_set")
public class SecuritySetting extends TO {
    @Id
    private int id;

    @Column(name = "user_id")
    private int userId;

    @Column(name = "can_view_profile")
    private int profileAccessLevel;

    @Column(name = "can_view_freinds")
    private int friendsAccessLevel;

    @Column(name = "can_post_private")
    private int privateAccessLevel;

    @Column(name = "can_post_to_wall")
    private int postWallAccessLevel;

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getProfileAccessLevel() {
        return profileAccessLevel;
    }

    public void setProfileAccessLevel(int profileAccessLevel) {
        this.profileAccessLevel = profileAccessLevel;
    }

    public int getFriendsAccessLevel() {
        return friendsAccessLevel;
    }

    public void setFriendsAccessLevel(int friendsAccessLevel) {
        this.friendsAccessLevel = friendsAccessLevel;
    }

    public int getPrivateAccessLevel() {
        return privateAccessLevel;
    }

    public void setPrivateAccessLevel(int privateAccessLevel) {
        this.privateAccessLevel = privateAccessLevel;
    }

    public int getPostWallAccessLevel() {
        return postWallAccessLevel;
    }

    public void setPostWallAccessLevel(int postWallAccessLevel) {
        this.postWallAccessLevel = postWallAccessLevel;
    }
}