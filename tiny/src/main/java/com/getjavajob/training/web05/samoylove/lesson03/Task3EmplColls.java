package com.getjavajob.training.web05.samoylove.lesson03;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

public class Task3EmplColls {
    private HashMap<String, Employee> employeesMap;
    private HashMap<String, Manager> managersMap;

    public Task3EmplColls() {
    }

    public HashMap<String, Employee> getEmployeesMap() {
        return employeesMap;
    }

    public void setEmployeesMap(HashMap<String, Employee> employeesMap) {
        this.employeesMap = employeesMap;
    }

    public HashMap<String, Manager> getManagersMap() {
        return managersMap;
    }

    public void setManagersMap(HashMap<String, Manager> managersMap) {
        this.managersMap = managersMap;
    }

    private <T extends Person> T find(String name) {
        if (name == null) {
            throw new NullPointerException();
        }
        T person = (T) employeesMap.get(name);
        if (person == null) {
            person = (T) managersMap.get(name);
        }
        return person;
    }

    public Collection<Manager> getAllManagers() {
        return new LinkedList<>(managersMap.values());
    }

    public LinkedList<Employee> getSubordinates(String managerName) throws IllegalArgumentException {
        Manager manager = managersMap.get(managerName);
        if (manager != null) {
            return manager.getEmployeesUnder();
        } else {
            throw new IllegalArgumentException("Manager not found");
        }
    }

    public void addSubordinate(String managerName, Employee... employees) throws IllegalArgumentException {
        Manager manager;
        try {
            manager = find(managerName);
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("This is not a manager");
        }
        if (manager != null) {
            for (Employee employee : employees) {
                if (employee != null && !manager.getEmployeesUnder().contains(employee)) {
                    manager.addEmployee(employee);
                } else {
                    throw new IllegalArgumentException("Manager already has adding employee");
                }
            }
        } else {
            throw new IllegalArgumentException("Manager not found");
        }
    }

    public void removeEmployee(String managerName, String employeeName) throws IllegalArgumentException {
        Manager manager;
        Employee employee;
        try {
            manager = find(managerName);
            employee = find(employeeName);
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("The manager entered is not a manager or the employee is not employee");
        }
        if (manager != null && employee != null) {
            if (manager.getEmployeesUnder().contains(employee)) {
                manager.removeEmployee(employee);
            } else {
                throw new IllegalArgumentException("Manager hadn't such employee");
            }
        } else {
            throw new IllegalArgumentException("Manager or employee not found");
        }
    }
}