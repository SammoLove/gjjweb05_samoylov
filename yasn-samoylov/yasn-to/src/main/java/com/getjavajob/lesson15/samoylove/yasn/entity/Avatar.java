package com.getjavajob.lesson15.samoylove.yasn.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Avatar extends TO {
	@Column(name = "mime_type", nullable = false)
	String mimeType;
	@Column(name = "image_file", nullable = false)
	byte[] file;
	@Id
	@Column(name = "user_id", unique = true, nullable = false)
	private int id;

	//@JoinColumn(name = "ID")
	//@OneToOne
	//@MapsId
	//User user;

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}
}