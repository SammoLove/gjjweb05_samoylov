package com.getjavajob.training.web05.samoylove.lesson15;

import com.getjavajob.training.web05.samoylove.lesson13.ContactDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

public class ContactsServlet extends HttpServlet {
    private static ContactDAO contactDAO;

    @Override
    public void init() throws ServletException {
        super.init();
        contactDAO = new ContactDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");
        final PrintWriter out = resp.getWriter();
        String query = req.getParameter("query");
        if (query == null) {
            try {
                final String result = contactDAO.search(null).toString();
                resp.setStatus(200);
                out.println(result);
            } catch (Exception e) {
                resp.setStatus(500);
                out.println("Error: " + e.getMessage());
            }
        } else {
            try {
                final String result = contactDAO.search(query).toString();
                resp.setStatus(200);
                out.println(result);
            } catch (Exception e) {
                resp.setStatus(500);
                out.println("Error: " + e.getMessage());
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String body = getBody(req);
        if (body.length() > 10) {
            final String[] flp = body.split(" ");
            try {
                final int newId = contactDAO.add(flp[0], flp[1], flp[2]);
                resp.setStatus(201);
                resp.setHeader("X-NewContactID", String.valueOf(newId));
            } catch (Exception e) {
                resp.setStatus(500);
                resp.getWriter().println("Error: " + e.getMessage());
            }
        } else {
            resp.setStatus(400);
            resp.getWriter().println("Wrong input!");
        }
    }

    private String getBody(HttpServletRequest req) {
        String body;
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader;

        try (InputStream inputStream = req.getInputStream()) {
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        body = stringBuilder.toString();
        return body;
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String query = req.getParameter("id");
        if (query != null) {
            try {
                contactDAO.delete(Integer.parseInt(query));
            } catch (Exception e) {
                resp.setStatus(500);
                resp.getWriter().println("Error: " + e.getMessage());
            }
            resp.setStatus(204);
        } else {
            resp.setStatus(400);
        }

    }

    @Override
    public void destroy() {
        contactDAO.close();
        super.destroy();
    }
}