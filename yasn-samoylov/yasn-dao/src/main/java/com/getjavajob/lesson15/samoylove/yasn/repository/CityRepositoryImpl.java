package com.getjavajob.lesson15.samoylove.yasn.repository;

import com.getjavajob.lesson15.samoylove.yasn.entity.City;
import org.springframework.stereotype.Repository;

@Repository
public class CityRepositoryImpl extends AbstractCommonRepository<City> implements CityRepository {
    @Override
    protected Class<City> getEntityClass() {
        return City.class;
    }
}