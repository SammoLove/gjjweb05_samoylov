package com.getjavajob.training.web05.samoylove.lesson08;

public class XmlParserFactory {
    private static final Object lock = new Object();
    private static XmlParser instance = null; //volatile used for mutable objects

    private XmlParserFactory() {
    }

    public static XmlParser newXmlParser() {
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) {
                    final long mem = Runtime.getRuntime().freeMemory();
                    if (mem < 10 * 1024 * 1024) {
                        instance = new STAXParser();
                    } else if (mem < 100 * 1024 * 1024) {
                        instance = new SAXParser();
                    } else {
                        instance = new DOMParser();
                    }
                }
            }
        }
        return instance;
    }
}