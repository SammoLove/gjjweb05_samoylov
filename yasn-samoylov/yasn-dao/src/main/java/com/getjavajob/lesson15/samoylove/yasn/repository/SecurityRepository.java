package com.getjavajob.lesson15.samoylove.yasn.repository;

import com.getjavajob.lesson15.samoylove.yasn.entity.SecuritySetting;
import org.springframework.stereotype.Repository;

@Repository
public interface SecurityRepository extends DAO<SecuritySetting>, CommonDBOperations<SecuritySetting> {
}