package com.getjavajob.training.web05.samoylove.lesson04;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;

public class Task1Test {
    @Test
    public void testFindAllTestClasses() {
        Collection<Class<?>> classCol = Task1.findAllTestClasses();
        Assert.assertEquals(classCol, Arrays.asList(Task1.class));
    }
}
