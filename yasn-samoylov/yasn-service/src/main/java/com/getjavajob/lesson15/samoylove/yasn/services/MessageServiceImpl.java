package com.getjavajob.lesson15.samoylove.yasn.services;

import com.getjavajob.lesson15.samoylove.yasn.entity.Interlocutor;
import com.getjavajob.lesson15.samoylove.yasn.entity.Message;
import com.getjavajob.lesson15.samoylove.yasn.entity.User;
import com.getjavajob.lesson15.samoylove.yasn.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class MessageServiceImpl implements MessageService {
	private MessageRepository messageRepository;

	@Autowired
	public MessageServiceImpl(MessageRepository messageRepository) {
		this.messageRepository = messageRepository;
	}

	@Override
	@Transactional
	public void sendPrivate(User sender, User receiver, String message) {
		messageRepository.createPrivate(sender.getId(), receiver.getId(), message);
	}

	@Override
	@Transactional
	public void sendPublic(User sender, User receiver, String message) {
		messageRepository.createPublic(sender.getId(), receiver.getId(), message);
	}

	@Override
	@Transactional
	public void markAsRead(int... msgId) {
		messageRepository.setReaded(msgId);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Interlocutor> getInterlocutors(User user) {
		return messageRepository.getInterlocutors(user);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Message> getCorrespondence(User user, Interlocutor interlocutor) {
		final List<Message> correspondence = messageRepository.getCorrespondence(user, interlocutor);
		return clearUselessInfo(correspondence);
	}

	/**
	 * @param correspondence Clearing correspondence from user info to return message list
	 *                       without User object info, only id, name and nick are save
	 * @return list of cleared correspondense
	 */
	private List<Message> clearUselessInfo(final List<Message> correspondence) {
		for (Message message : correspondence) {
			final User sender = message.getSender();
			sender.setAge(0);
			sender.setAvatar(null);
			sender.setBirth(new Date());
			sender.setBirthLocation(0);
			sender.setCurrLocation(0);
			sender.setEmail(null);
			sender.setGender(null);
			sender.setPassword(null);
			sender.setFriends(new TreeSet<User>());
			message.setSender(sender);

			final User receiver = new User();
			receiver.setFriends(new TreeSet<User>());
			receiver.setAge(0);
			receiver.setBirthLocation(0);
			receiver.setCurrLocation(0);
			message.setReceiver(receiver); // we don't need to know receiver info into dialog
		}
		return correspondence;
	}

	@Override
	@Transactional(readOnly = true)
	public Integer getUnreadCount(User user) {
		return messageRepository.getUnreadCount(user).intValue();
	}

	@Deprecated
	@Transactional(readOnly = true)
	public Map<User, List<Message>> getAllOf(User user) {
		final List<Message> allOf = messageRepository.getAllOf(user);
		final Map<User, List<Message>> result = new HashMap<>();
		if (allOf != null && !allOf.isEmpty()) {
			for (Message message : allOf) {
				final User receiver = message.getReceiver();
				final User sender = message.getSender();
				final User interlocutor = receiver.getId() == user.getId() ? sender : receiver;

				List<Message> messagesOfInterlocutor = result.get(interlocutor);
				if (messagesOfInterlocutor == null) {
					messagesOfInterlocutor = new LinkedList<>();
				}
				messagesOfInterlocutor.add(message);
				result.put(interlocutor, messagesOfInterlocutor);
			}
		}
		return result;
	}

	@Deprecated
	@Transactional(readOnly = true)
	public List<Message> readInbox(User user) {
		return messageRepository.getInbox(user);
	}

	@Deprecated
	@Transactional(readOnly = true)
	public List<Message> readOutbox(User user) {
		return messageRepository.getOutbox(user);
	}
}