package com.getjavajob.lesson15.samoylove.yasn.web;

import com.getjavajob.lesson15.samoylove.yasn.entity.User;
import com.getjavajob.lesson15.samoylove.yasn.services.UserService;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class IsAuthFilter implements Filter {
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        final HttpSession session = ((HttpServletRequest) req).getSession();

        final Boolean authFiltered = (Boolean) session.getAttribute("authFiltered"); //flag said that I already was here
        final Boolean isStatic = (Boolean) req.getAttribute("isStatic"); //this flag could be set by SkipStaticFilter
        if (isStatic != null && isStatic) { // if it is static - skip filtration
            //req.removeAttribute("isStatic");
            //req.removeAttribute("authFiltered");
            chain.doFilter(req, resp);
            return;
        }

        final WebApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(req.getServletContext());
        final UserInfoSessionStorage userInfoSessionStorage = context.getBean(UserInfoSessionStorage.class);
        final int loggedUserId = userInfoSessionStorage.getUserId();
        if (authFiltered != null && authFiltered) { //if user already was at register form
            if (loggedUserId > 0 || ((HttpServletRequest) req).getRequestURI().startsWith("/login")) { // and he already authorized or now is passing authorization - skip
                chain.doFilter(req, resp);
                return;
            } else { // but if he isn't authorized, repeat
                req.removeAttribute("authFiltered");
                //todo подумать над этим блоком
            }
        }

        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");

        if (loggedUserId == 0) { //if not info in session, find in cookies
            final Cookie[] cookies = ((HttpServletRequest) req).getCookies();
            if (cookies != null && cookies.length > 1) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("userId")) {
                        final Integer id = Integer.valueOf(cookie.getValue());
                        userInfoSessionStorage.setUserId(id);
                        final UserService userService = context.getBean(UserService.class);
                        User user = userService.read(id);
                        userInfoSessionStorage.setUserName(user.getName());
                        // ?
                        chain.doFilter(req, resp);
                    }
                }
            } else { // if not in cookies too - redirect to login
                session.setAttribute("authFiltered", Boolean.TRUE); // чтоб не зациклилось
                //final RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
                //requestDispatcher.forward(req, resp); //include?
                String requestURI = ((HttpServletRequest) req).getRequestURI();
                if (requestURI.equals("/login")) {
                    requestURI = "";
                } else {
                    requestURI = "?returnUrl=" + requestURI;
                }
                HttpServletResponse httpResponse = (HttpServletResponse) resp;
                httpResponse.sendRedirect("/login" + requestURI);
            }
        } else { // loggedUserId active (id in session not 0) - skip
            chain.doFilter(req, resp);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, filterConfig.getServletContext());
    }

    @Override
    public void destroy() {
    }
}