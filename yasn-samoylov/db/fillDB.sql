INSERT INTO country (name) VALUES
  ('USA'),
  ('UK'),
  ('Australia'),
  ('Canada'),
  ('France');

INSERT INTO city (name, country_id) VALUES
  ('NY', 1), ('LA', 1), ('Tuscon', 1), ('Houston', 1), ('Detroit', 1),
  ('London', 2), ('Oxford', 2), ('Manchester', 2), ('Liverpool', 2),
  ('Sydney', 3), ('Brisbane', 3), ('Melbourne', 3), ('Perth', 3),
  ('Vancouver', 4), ('Winnipeg', 4), ('Toronto', 4), ('Quebec', 4),
  ('Paris', 5), ('Lyons', 5), ('Marseille', 5), ('Toulouse', 5);

INSERT INTO usert (name, nickname, gender, email, birth, birth_location, curr_location, password) VALUES
  ('Sacha Baron Cohen', 'AliG', TRUE, 'a1@ya.ru', DATE '8.1.76', 2, 3, '111'),
  ('Charlize Theron', 'ImperatorFuriosa', FALSE, 'a2@ya.ru', DATE '1.6.79', 4, 4, '222'),
  ('Jamie Bell', 'BillyElliot', TRUE, 'a3@ya.ru', DATE '2.12.1980', 4, 1, '333'),
  ('Jennifer Connelly', 'MarionSilver', FALSE, 'a4@ya.ru', DATE '1.06.1978', 1, 2, '444'),
  ('Aamir Khan', 'PK', TRUE, 'a5@ya.ru', DATE '21.12.1992', 2, 6, '555'),
  ('Rachel Weisz', 'IzziCreo', FALSE, 'a6@ya.ru', DATE '12.02.1995', 5, 7, '666'),
  ('William Fichtner', 'Alex Mahone', TRUE, 'a7@ya.ru', DATE '1.06.1993', 4, 7, '777'),
  ('Lily Cole', 'Valentina', FALSE, 'a8@ya.ru', DATE '1.03.1989', 7, 6, '888'),
  ('Benedict Cumberbatch', 'Sherlock', TRUE, 'a9@ya.ru', DATE '21.12.1992', 6, 5, '999'),
  ('Natalie Portman', '', FALSE, 'a10@ya.ru', DATE '3.06.1982', 3, 3, '000'),
  ('Paul Bettany', 'TomEdison', TRUE, 'a11@ya.ru', DATE '1.06.1989', 6, 6, '111'),
  ('Audrey Tautou', 'Amelie', FALSE, 'a12@ya.ru', DATE '6.06.1991', 4, 4, '222'),
  ('Jared Leto', 'Nemo', TRUE, 'a13@ya.ru', DATE '21.12.1992', 2, 1, '333'),
  ('Amanda Seyfried', 'SylviaWeis', FALSE, 'a14@ya.ru', DATE '1.06.1983', 1, 2, '444'),
  ('Jude Law', 'TedPikul', TRUE, 'a15@ya.ru', DATE '13.06.1985', 8, 9, '555'),
  ('Saoirse Ronan', 'Hanna', FALSE, 'a16@ya.ru', DATE '16.06.1986', 10, 11, '666'),
  ('Edward Norton', '', TRUE, 'a17@ya.ru', DATE '21.12.1992', 12, 8, '777'),
  ('Marion Cotillard', 'LillyBertineau', FALSE, 'a18@ya.ru', DATE '1.06.1989', 8, 9, '888'),
  ('Kevin Spacey', 'Prot', TRUE, 'a19@ya.ru', DATE '11.06.1988', 4, 10, '999');

INSERT INTO level (id, descr) VALUES (0, 'Everyone'), (1, 'Registered'), (2, 'Friends'), (3, 'None');

INSERT INTO private_set (user_id, can_view_profile, can_view_freinds, can_post_private, can_post_to_wall) VALUES
  (0, 0, 0, 0, 0),
  (1, 1, 1, 1, 1),
  (2, 2, 2, 2, 2),
  (3, 3, 3, 3, 3);

INSERT INTO message (body, time, user_id, author_user_id, readed, priv) VALUES
  ('Hi. Let"s go to 9.', TIME '2015-11-17 11:23:02', 1, 7, TRUE, TRUE),
  ('How much?', TIME '2015-11-17 11:26:02', 7, 1, TRUE, TRUE),
  ('Something about pair of million', TIME '2015-11-17 11:26:58', 1, 7, TRUE, TRUE),
  ('Pf, go yourself for only 2 mln :)', TIME '2015-11-17 12:01:08', 7, 1, FALSE, TRUE);