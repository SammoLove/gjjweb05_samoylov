package com.getjavajob.lesson15.samoylove.yasn.web;

import com.getjavajob.lesson15.samoylove.yasn.entity.Avatar;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class FileValidator implements Validator {

    public boolean supports(Class<?> clazz) {
        return Avatar.class.isAssignableFrom(clazz);
    }

    public void validate(Object obj, Errors errors) {
        Avatar avatar = (Avatar) obj;
        if (avatar.getFile() != null) {
            if (avatar.getFile().length == 0) {
                errors.rejectValue("avatar", "missing or empty image file");
            }
        }
    }
}