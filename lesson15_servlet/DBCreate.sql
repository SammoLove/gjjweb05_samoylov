CREATE DATABASE contactsDB;
CREATE TABLE contacts
(
  user_id          SERIAL      NOT NULL UNIQUE,
  first_name       VARCHAR(16) NOT NULL,
  last_name        CHARACTER VARYING(16),
  primary_phone_id INT,

  PRIMARY KEY (user_id)
);

CREATE TABLE phones
(
  phone_id SERIAL      NOT NULL UNIQUE,
  user_id  INT         NOT NULL,
  phone    VARCHAR(16) NOT NULL,
  type     VARCHAR(4) DEFAULT 'mob',

  PRIMARY KEY (phone_id),
  FOREIGN KEY (user_id) REFERENCES contacts (user_id) ON DELETE CASCADE
);

INSERT INTO contacts VALUES (1, 'Анна', 'Полянская', 3);
INSERT INTO phones VALUES (1, 1, '+11000', 'mob');
INSERT INTO phones VALUES (2, 1, '+12000', 'home');
INSERT INTO phones VALUES (3, 1, '+13000', 'mob');

INSERT INTO contacts VALUES (2, 'Юрий', 'Самолетов', 5);
INSERT INTO phones VALUES (4, 2, '+21000', 'work');
INSERT INTO phones VALUES (5, 2, '+22000', 'work');

INSERT INTO contacts VALUES (3, 'Алексей', 'Китайцев', 6);
INSERT INTO phones VALUES (6, 3, '+31000', 'mob');
INSERT INTO phones VALUES (7, 3, '+32000', 'home');
INSERT INTO phones VALUES (8, 3, '+33000', 'work');
INSERT INTO phones VALUES (9, 3, '+34000', 'fax');

INSERT INTO contacts VALUES (4, 'Ольга', 'Коусова', 10);
INSERT INTO phones VALUES (10, 4, '+41000', 'mob');
INSERT INTO phones VALUES (11, 4, '+42000', 'mob');

INSERT INTO contacts VALUES (5, 'Алекс', 'Чебан', 12);
INSERT INTO phones VALUES (12, 5, '+51000', 'work');

INSERT INTO contacts VALUES (6, 'Юля', 'Полянская', 14);
INSERT INTO phones VALUES (13, 6, '+61000', 'work');
INSERT INTO phones VALUES (14, 6, '+62000', 'fax');

ALTER TABLE contacts OWNER TO admin;
ALTER TABLE phones OWNER TO admin;