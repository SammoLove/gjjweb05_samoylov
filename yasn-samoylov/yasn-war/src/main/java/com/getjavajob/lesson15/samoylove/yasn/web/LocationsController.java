package com.getjavajob.lesson15.samoylove.yasn.web;

import com.getjavajob.lesson15.samoylove.yasn.entity.City;
import com.getjavajob.lesson15.samoylove.yasn.services.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LocationsController {
    @Autowired
    LocationService locationService;

    @RequestMapping(path = "/getCities/{countryId}", method = RequestMethod.GET)
    public ModelAndView getCitiesListForAjax(@PathVariable int countryId) {
        final Iterable<City> citiesOfCountry = locationService.getCitiesByCountryId(countryId);
        return new ModelAndView("citiesList", "citiesList", citiesOfCountry);
    }
}