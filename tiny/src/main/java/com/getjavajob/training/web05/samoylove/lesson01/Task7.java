package com.getjavajob.training.web05.samoylove.lesson01;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Task7 {
    public static <T> Set<Set<T>> powerSet(Set<T> inputSet) {
        Set<Set<T>> outputSetOfSets = new HashSet<>();

        if (inputSet.isEmpty()) {
            outputSetOfSets.add(new HashSet<T>());
            return outputSetOfSets;
        }

        List<T> list = new ArrayList<>(inputSet);
        T head = list.get(0);
        Set<T> rest = new HashSet<>(list.subList(1, list.size()));
        for (Set<T> set : powerSet(rest)) {
            Set<T> newSet = new HashSet<>();
            newSet.add(head);
            newSet.addAll(set);
            outputSetOfSets.add(newSet);
            outputSetOfSets.add(set);
        }
        return outputSetOfSets;
    }
}