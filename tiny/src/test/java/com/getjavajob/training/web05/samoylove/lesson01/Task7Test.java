package com.getjavajob.training.web05.samoylove.lesson01;

import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class Task7Test {

    @SuppressWarnings("unchecked")
    @Test
    public void testPowerSets() throws NullPointerException {
        Set<Integer> set = Sets.newHashSet(1, 2, 3);
        HashSet<HashSet<?>> right = Sets.newHashSet(
                Sets.newHashSet(),
                Sets.newHashSet(1),
                Sets.newHashSet(2),
                Sets.newHashSet(3),
                Sets.newHashSet(1, 2),
                Sets.newHashSet(1, 3),
                Sets.newHashSet(2, 3),
                Sets.newHashSet(1, 2, 3));

        Set<Set<Integer>> result = Task7.powerSet(set);

        Assert.assertEquals(right, result);
    }
}