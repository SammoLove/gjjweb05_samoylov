$(document).ready(function (){
	$('#form').change(function (){
		showNewResults();
	});
});

function showNewResults(){
	$.get("/search/ajax" +
		"?partName=" + (!$("#partName").val() ? "" : $("#partName").val()) +
		"&type=" + (!$("#type").val() ? "" : $("#type").val()) +
		"&gender=" + (!$("#gender").val() ? "" : $("#gender").val()) +
		"&minAge=" + (!$("#minAge").val() ? "" : $("#minAge").val()) +
		"&maxAge=" + (!$("#maxAge").val() ? "" : $("#maxAge").val()) +
		"&birthCountry=" + (!$("#birthCountry").val() ? "" : $("#birthCountry").val()) +
		"&mybc=" + (!$("#mybc").val() ? "" : $("#mybc").val()) +
		"&birthCity=" + (!$("#birthCity").val() ? "" : $("#birthCity").val()) +
		"&mybct=" + (!$("#mybct").val() ? "" : $("#mybct").val()) +
		"&currCountry=" + (!$("#currCountry").val() ? "" : $("#currCountry").val()) +
		"&mycc=" + (!$("#mycc").val() ? "" : $("#mycc").val()) +
		"&currCity=" + (!$("#currCity").val() ? "" : $("#currCity").val()) +
		"&mycct=" + (!$("#mycct").val() ? "" : $("#mycct").val()) +
		"&submit=AjaxSearch",
		function (data){
			$("#searchResults").empty();
			for (var i = 0; i < data.length; i++)
				$("#searchResults").append(
					"<div class='searched-user'>" + i + "." +
					"<a href='/wall/id" + data[i].id + "'>" + data[i].name + "</a>, " +
					data[i].age + "</div>"
				);
		})
}