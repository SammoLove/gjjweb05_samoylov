package com.getjavajob.training.web05.samoylove.lesson06;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import static com.getjavajob.training.web05.samoylove.lesson06.Task1.fileFastCopy;
import static com.getjavajob.training.web05.samoylove.lesson06.Task1.fileSlowCopy;


public class Task1Test {
    TemporaryFolder tmp = new TemporaryFolder();
    File from;
    File to;

    @Before
    public void setUp() throws IOException {
        tmp.create();
        from = tmp.newFile("fromExample.file");
        to = tmp.newFile("toExample.file");
        try {
            to.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream fromStream = new FileOutputStream(from);
        int fileSize = 2 * 1024 * 1024;
        int bufferSize = 4 * 1024;
        byte[] buffer = new byte[bufferSize];
        final Random random = new Random();
        int written = 0;
        while (fileSize > written) {
            random.nextBytes(buffer);
            fromStream.write(buffer);
            written += buffer.length;
        }
    }

    @Test
    public void testFileCopy() throws IOException, NoSuchAlgorithmException {
        to.delete();
        fileSlowCopy(from, to);

        Assert.assertEquals(from.length(), to.length());
        Assert.assertEquals(getSha256(from), getSha256(to));
    }

    @Test
    public void testFileFastCopy() throws IOException, NoSuchAlgorithmException {
        to.delete();
        fileFastCopy(from, to);

        Assert.assertEquals(from.length(), to.length());
        Assert.assertEquals(getSha256(from), getSha256(to));
    }

    @After
    public void tearDown() {
        from.delete();
        to.delete();
        tmp.delete();
    }

    private String getSha256(final File file) throws IOException, NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        FileInputStream fis = new FileInputStream(file);
        byte[] dataBytes = new byte[1024];
        int nread;
        while ((nread = fis.read(dataBytes)) != -1) {
            md.update(dataBytes, 0, nread);
        }
        byte[] mdbytes = md.digest();

        StringBuilder hexString = new StringBuilder();
        for (byte mdbyte : mdbytes) {
            hexString.append(Integer.toHexString(0xFF & mdbyte));
        }
        return hexString.toString();
    }
}