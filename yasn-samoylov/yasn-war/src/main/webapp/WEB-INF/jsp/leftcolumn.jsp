<div class="col-sm-3" id="left-menu">
    <div class="row" id="logged-info">
        <div class="col-sm-2">
            <img class="img-circle" src="/img/default-photo.jpg" width="30" height="45">
        </div>
        <div class="col-sm-8">
            <%--@elvariable id="userInfoSessionStorage" type="com.getjavajob.lesson15.samoylove.yasn.web.UserInfoSessionStorage"--%>
            <p style="margin-bottom: 0.5em"><c:out value="${userInfoSessionStorage.userName}"/></p>

            <p><a href="/logout">Logout</a></p>
        </div>
    </div>

    <nav>
        <ul class="list-group">
            <li class="list-group-item"><i class="glyphicon glyphicon-home"></i> <a
                    href="/wall/id<c:out value="${userInfoSessionStorage.userId}"/>">Home</a></li>
            <li class="list-group-item"><i class="glyphicon glyphicon-comment"></i> <a
                    href="/msgs">Messages</a>
                <span class="badge" id="unread-count">-
                    <span class="glyphicon glyphicon-refresh" id="msg-count-refresh"></span>
                </span></li>
            <li class="list-group-item"><i class="glyphicon glyphicon-link"></i> Friends</li>
            <li class="list-group-item"><i class="glyphicon glyphicon-search"></i> <a
                    href="<c:out value="${pageContext.request.contextPath}"/>/search">Advanced search</a></li>
            <li class="list-group-item"><a href="<c:out value="${pageContext.request.contextPath}"/>/edit"><i
                    class="glyphicon glyphicon-cog"></i> Profile edit</a></li>
            <li class="list-group-item"><i class="glyphicon glyphicon-eye-close"></i> Security edit</li>
        </ul>
    </nav>
</div>
<!--end left-menu-->
