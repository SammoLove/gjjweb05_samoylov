package com.getjavajob.training.web05.samoylove.lesson06;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.*;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class Task2 {
    private static Map<String, Collection<Product>> priceList = new LinkedHashMap<>();

    public static Map<String, Collection<Product>> loadGoodsByDOM(final File XMLFile) {
        Document document = null;
        try {
            final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            document = documentBuilder.parse(XMLFile);
        } catch (IOException | SAXException | ParserConfigurationException e) {
            e.printStackTrace();
        }

        assert document != null;
        final NodeList nodes = document.getElementsByTagName("vehicle");

        Collection<Product> products = null;
        String lastCategory = "";

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                final String category = node.getParentNode().getAttributes().item(0).getTextContent();
                final String model = element.getElementsByTagName("name").item(0).getTextContent();
                final int quantity = Integer.parseInt(element.getElementsByTagName("qty").item(0).getTextContent());
                final double price = Double.parseDouble(element.getElementsByTagName("price").item(0).getTextContent());

                if (category.equals(lastCategory)) {
                    assert products != null;
                    products.add(new Product(model, quantity, price));
                } else {
                    if (products != null) {
                        priceList.put(lastCategory, products);
                    }
                    products = new ArrayList<>();
                    products.add(new Product(model, quantity, price));
                    lastCategory = category;
                }
            }
        }
        priceList.put(lastCategory, products);
        return priceList;
    }


    public static Map<String, Collection<Product>> loadGoodsBySAX(final File XMLFile) {
        final SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        final SAXParser saxParser;
        XMLReader xmlReader;
        try (InputStream fileStream = new FileInputStream(XMLFile)) {
            saxParser = saxParserFactory.newSAXParser();
            xmlReader = saxParser.getXMLReader();

            ContentHandler handler = new DefaultHandler() {
                Collection<Product> products = null;
                String category;
                boolean modelWaited;
                String model;
                boolean quantityWaited;
                int quantity;
                boolean priceWaited;
                double price;

                @Override
                public void startElement(final String uri,
                                         final String localName,
                                         final String qName,
                                         final Attributes attributes) throws SAXException {
                    switch (qName) {
                        case "category":
                            category = attributes.getValue(0);
                            products = new ArrayList<>();
                            break;
                        case "name":
                            modelWaited = true;
                            break;
                        case "qty":
                            quantityWaited = true;
                            break;
                        case "price":
                            priceWaited = true;
                            break;
                    }
                }

                @Override
                public void endElement(final String uri,
                                       final String localName,
                                       final String qName) throws SAXException {
                    switch (qName) {
                        case "vehicle":
                            products.add(new Product(model, quantity, price));
                            break;
                        case "category":
                            priceList.put(category, products);
                            break;
                    }
                }

                @Override
                public void characters(final char[] ch,
                                       final int start,
                                       final int length) throws SAXException {
                    final String text = new String(ch, start, length).trim();
                    if (!"".equals(text)) {
                        if (modelWaited) {
                            model = text;
                            modelWaited = false;
                        } else if (quantityWaited) {
                            quantity = Integer.parseInt(text);
                            quantityWaited = false;
                        } else if (priceWaited) {
                            price = Integer.parseInt(text);
                            priceWaited = false;
                        }
                    }
                }
            };

            xmlReader.setContentHandler(handler);
            xmlReader.parse(new InputSource(fileStream));
        } catch (IOException | SAXException | ParserConfigurationException e) {
            e.printStackTrace();
        }
        return priceList;
    }


    public static Map<String, Collection<Product>> loadGoodsBySTAX(final File XMLFile) {
        try (InputStream fileStream = new FileInputStream(XMLFile)) {
            XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
            XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(fileStream);

            Collection<Product> products = null;
            String category = null;
            boolean modelWaited = false;
            String model = null;
            boolean quantityWaited = false;
            int quantity = 0;
            boolean priceWaited = false;
            double price = 0;
            int event;
            do {
                event = reader.next();
                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:
                        switch (reader.getName().getLocalPart()) {
                            case "category":
                                category = reader.getAttributeValue(0);
                                products = new ArrayList<>();
                                break;
                            case "name":
                                modelWaited = true;
                                break;
                            case "qty":
                                quantityWaited = true;
                                break;
                            case "price":
                                priceWaited = true;
                                break;
                        }
                        break;
                    case XMLStreamConstants.CHARACTERS:
                        final String text = reader.getText().trim();
                        if (!"".equals(text)) {
                            if (modelWaited) {
                                model = text;
                                modelWaited = false;
                            } else if (quantityWaited) {
                                quantity = Integer.parseInt(text);
                                quantityWaited = false;
                            } else if (priceWaited) {
                                price = Integer.parseInt(text);
                                priceWaited = false;
                            }
                        }

                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        switch (reader.getName().getLocalPart()) {
                            case "vehicle":
                                products.add(new Product(model, quantity, price));
                                break;
                            case "category":
                                priceList.put(category, products);
                                break;
                        }
                        break;
                }
            } while (!reader.hasNext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return priceList;
    }


    public static class Product {
        private String model;
        private int quantity;
        private double price;

        public Product(String model, int quantity, double price) {
            this.model = model;
            this.quantity = quantity;
            this.price = price;
        }

        @Override
        public boolean equals(Object obj) {
            Product product = (Product) obj;
            return product.model.equals(this.model) && product.quantity == this.quantity && product.price == this.price;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }
    }
}