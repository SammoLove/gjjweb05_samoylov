<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--@elvariable id="citiesList" type="java.util.List"--%>
<c:forEach var="city" items="${citiesList}" varStatus="ctr">
    <option value="<c:out value="${city.id}"/>"><c:out value="${city.name}"/></option>
</c:forEach>