package com.getjavajob.training.web05.samoylove.lesson06;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Task1 {

    @SuppressWarnings("InfiniteLoopStatement")
    public static void fileSlowCopy(final File from, final File to) {
        try {
            to.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (FileInputStream fromStream = new FileInputStream(from);
             FileOutputStream toStream = new FileOutputStream(to)) {
            int b;
            while ((b = fromStream.read()) != -1) {
                toStream.write(b);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void fileFastCopy(final File from, final File to) {
        int bufferSize = 4 * 1024; //cluster size
        try {
            to.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (FileInputStream fromStream = new FileInputStream(from);
             FileOutputStream toStream = new FileOutputStream(to)) {
            byte[] buffer = new byte[bufferSize];
            int beenRead;
            while ((beenRead = fromStream.read(buffer)) > 0) {
                toStream.write(buffer, 0, beenRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
