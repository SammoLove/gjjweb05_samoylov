package com.getjavajob.training.web05.samoylove.lesson13;

import java.util.Comparator;

public class SortPhonesByType implements Comparator<Phone> {

    @Override
    public int compare(Phone o1, Phone o2) {
        return o1.getPhoneType().compareTo(o2.getPhoneType()) + (o1.getId() - o2.getId());
    }
}