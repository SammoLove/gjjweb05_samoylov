package com.getjavajob.lesson15.samoylove.yasn.web;

public enum AccessLevel {
    EVERYONE(0), REGISTERED(1), FRIENDS(2), NONE(3);

    private int value;

    AccessLevel(int value) {
        this.value = value;
    }

    static public AccessLevel getType(int aType) {
        for (AccessLevel type : AccessLevel.values()) {
            if (type.getValue() == (aType)) {
                return type;
            }
        }
        throw new RuntimeException("unknown type");
    }

    static public AccessLevel getType(String aType) {
        for (AccessLevel type : AccessLevel.values()) {
            if (type.getValue() == (Integer.parseInt(aType))) {
                return type;
            }
        }
        throw new RuntimeException("unknown type");
    }

    public int getValue() {
        return value;
    }
}