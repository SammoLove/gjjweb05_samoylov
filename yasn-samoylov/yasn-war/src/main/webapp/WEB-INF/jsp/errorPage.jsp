<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>404</title>
	<%--<link href="<c:url value="/css/wall.css" />" rel="stylesheet">--%>
	<link href="<c:out value="${pageContext.request.contextPath}"/>/css/style.css" rel="stylesheet">
	<link href="<c:out value="${pageContext.request.contextPath}"/>/css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<div class="container" style="margin-top: 60px" id="page"> <!--width: 900px-->
	<div class="row">
		<%@ include file="/WEB-INF/jsp/leftcolumn.jsp" %>
		<div class="col-sm-9" id="page-content">

			<%--@elvariable id="errorMsg" type="java.lang.String"--%>
			<p><c:out value="${ errorMsg }"/></p>

			<p><a href="/wall/">Return to wall</a></p>


		</div>
		<!--end page-content-->
		<%@ include file="/WEB-INF/jsp/footer.jsp" %>
	<%--</div>--%>
</body>
</html>