package com.getjavajob.training.web05.samoylove.lesson04;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;

public class Task2 {

    public static <T> T deepCopy(final T obj) throws CannotCopyObjectException {
        if (obj == null) return null;
        T objCopy = getNewInstance(obj);
        final Field[] objFields = obj.getClass().getDeclaredFields();
        final Field[] objCopyFields = objCopy.getClass().getDeclaredFields();
        for (int i = 0; i < objFields.length; i++) {
            objCopyFields[i] = fieldCopy(objFields[i], objCopyFields[i], obj, objCopy);
        }
        return objCopy;
    }

    private static <T> Field fieldCopy(final Field objField, final Field objCopyField, final T obj, final T objCopy) {
        objField.setAccessible(true);
        objCopyField.setAccessible(true);
        boolean onlyShallow = false;

        if (objField.isAnnotationPresent(Shallow.class)) {
            if (objField.isAnnotationPresent(Transient.class)) {
                System.err.println("Warning: Field has both - @Shallow & @Transient annotations");
            }
            onlyShallow = true;
        } else if (objField.isAnnotationPresent(Transient.class)) {
            System.err.println("Warning: Field " + objField.getName() + " skipped");
            return null;
        }

        final int mod = objField.getModifiers();
        if ((mod & Modifier.STATIC) != 0 || (mod & Modifier.TRANSIENT) != 0) {
            System.err.println("Warning: Field " + objField.getName() + " skipped");
            return null;
        }

        try {
            final Object val = objField.get(obj);
            if (val == null || val.getClass().isEnum()) {
                return null;
            } else if (val.getClass().isPrimitive() || onlyShallow) {
                objCopyField.set(objCopy, objectCopy(val));
            } else if (val instanceof String) {
                objCopyField.set(objCopy, objectCopy(val));
            } else if (val instanceof Integer) {
                objCopyField.set(objCopy, objectCopy(val));
            } else if (val instanceof List<?>) {
                objCopyField.set(objCopy, objectCopy(val));
            } else if (val.getClass().isArray()) {
                objCopyField.set(objCopy, objectCopy(val));
            } else {
                throw new CannotCopyObjectException("Unhandled Field Type!");
            }
        } catch (IllegalAccessException e) {
            throw new CannotCopyObjectException("Why? Because " + e.toString(), e);
        }
        return objCopyField;
    }

    @SuppressWarnings("unchecked")
    private static <T> T objectCopy(final T val) {
        if (val == null || val.getClass().isEnum()) {
            return null;
        } else if (val.getClass().isPrimitive()) {
            return val;
        } else if (val instanceof String) {
            return (T) new String((String) val);
        } else if (val instanceof Integer) {
            return (T) new Integer(((Integer) val).intValue());
        } else if (val instanceof Double) {
            return (T) new Double(((Double) val).doubleValue());
        } else if (val instanceof List<?>) {
            List<T> fromList = (List<T>) val;
            List<T> toList = new ArrayList<>();
            for (int j = 0; j < fromList.size(); j++) {
                toList.add(j, objectCopy(fromList.get(j)));
            }
            return (T) toList;
        } else if (val.getClass().isArray()) {
            Class<?> compoenetType = val.getClass().getComponentType();
            if (compoenetType.isPrimitive()) {
                Object newArray = Array.newInstance(compoenetType, Array.getLength(val));
                System.arraycopy(val, 0, newArray, 0, Array.getLength(val));
                return (T) newArray;
            } else {
                Object newArray = Array.newInstance(compoenetType, Array.getLength(val));
                ;
                for (int j = 0; j < Array.getLength(val); j++) {
                    Array.set(newArray, j, objectCopy(Array.get(val, j)));
                }
                return (T) newArray;
            }
        } else {
            throw new CannotCopyObjectException("Unhandled Field Type!");
        }
    }

    @SuppressWarnings("unchecked")
    private static <T> T getNewInstance(final T obj) throws CannotCopyObjectException {
        T objCopy;
        try {
            final Constructor<T> constructor = (Constructor<T>) obj.getClass().getConstructor();
            try { // if default constructor exist, create new instance
                objCopy = constructor.newInstance();
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                throw new CannotCopyObjectException("Why? Because " + e.toString(), e);
            }
        } catch (NoSuchMethodException e) { // if default constructor don't exist
            throw new CannotCopyObjectException("Why? Because " + e.toString(), e);
        }
        return objCopy;
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    public @interface Shallow {
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    public @interface Transient {
    }

    public static class CannotCopyObjectException extends RuntimeException {
        public CannotCopyObjectException(final String s, final Throwable reason) {
            super(s, reason);
        }

        public CannotCopyObjectException(final String s) {
            super(s);
        }
    }
}