package com.getjavajob.lesson15.samoylove.yasn.services;

import com.getjavajob.lesson15.samoylove.yasn.entity.User;
import org.springframework.stereotype.Service;

@Service
public interface LoginService {
    User checkPassword(String email, String password);
}