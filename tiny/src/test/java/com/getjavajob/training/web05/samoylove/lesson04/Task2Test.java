package com.getjavajob.training.web05.samoylove.lesson04;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import static com.getjavajob.training.web05.samoylove.lesson04.Task2.CannotCopyObjectException;
import static com.getjavajob.training.web05.samoylove.lesson04.Task2.deepCopy;
import static org.junit.Assert.*;

public class Task2Test {
    ClassExample originalObject;

    @Before
    public void init() {
        originalObject = new ClassExample();
        originalObject.setA(2);
        ClassExample.setB(3);
        originalObject.setC(4);
        originalObject.setD("STR");
        originalObject.setdShallow("STR Shallow");
        originalObject.setdTransient("STR Transient");
        originalObject.setdTransientShallow("STR Transient Shallow");
        originalObject.setF(new LinkedList<>(Arrays.asList("one", "two", "3")));
        originalObject.setG(new ArrayList(Arrays.asList("abc", "def", "ghi")));
        originalObject.setH(new int[]{1, 2, 3, 4});
        originalObject.setI(new Double[]{10d, 20d, 30d, 40d});
    }

    @Test
    public void testDeepCopyNotJustClone() throws CannotCopyObjectException {
        init();
        final ClassExample anotherObject = deepCopy(originalObject);

        assertNotEquals(anotherObject, originalObject);
    }

    @Test
    public void testAnnotationsWorks() throws CannotCopyObjectException {
        init();
        final ClassExample anotherObject = deepCopy(originalObject);

        assertEquals(anotherObject.getdShallow(), originalObject.getdShallow());
        assertNull(anotherObject.getdTransient());
        assertEquals(anotherObject.getdTransientShallow(), originalObject.getdTransientShallow()); //with warning
    }

    @Test
    public void testShallowIsShallowDeepIsDeep() throws CannotCopyObjectException {
        init();
        final ClassExample anotherObject = deepCopy(originalObject);

        assertEquals(anotherObject.getD(), originalObject.getD());
        assertNotSame(anotherObject.getD(), originalObject.getD());
        assertEquals(anotherObject.getdShallow(), originalObject.getdShallow());
        assertNotSame(anotherObject.getdShallow(), originalObject.getdShallow());

    }

    @Test
    public void testListFieldsCopied() throws CannotCopyObjectException {
        init();
        final ClassExample anotherObject = deepCopy(originalObject);

        assertEquals(anotherObject.getF(), originalObject.getF());
        for (int i = 0; i < anotherObject.getF().size(); i++) {
            assertEquals(originalObject.getF().get(i), anotherObject.getF().get(i));
            assertNotSame(originalObject.getF().get(i), anotherObject.getF().get(i));
        }
    }

    @Test
    public void testPrimitivesArrayFieldsCopied() throws CannotCopyObjectException {
        init();
        final ClassExample anotherObject = deepCopy(originalObject);

        assertNotEquals(anotherObject.getH(), originalObject.getH());
        assertFalse(anotherObject.getH() == originalObject.getH());
        assertArrayEquals(anotherObject.getH(), originalObject.getH());
    }

    @Test
    public void testReferencesArrayFieldsCopied() throws CannotCopyObjectException {
        init();
        final ClassExample anotherObject = deepCopy(originalObject);

        assertNotEquals(anotherObject.getI(), originalObject.getI());
        assertFalse(anotherObject.getI() == originalObject.getI());
        assertArrayEquals(anotherObject.getI(), originalObject.getI());
        for (int i = 0; i < originalObject.getI().length; i++) {
            assertFalse(originalObject.getI()[i] == anotherObject.getI()[i]);
        }
    }
}