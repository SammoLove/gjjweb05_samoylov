package com.getjavajob.lesson15.samoylove.yasn.web;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserInfoSessionStorage {
    private int userId;
    private String userName;
    private Object pocketForObject;

    public UserInfoSessionStorage() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Object getPocketForObject() {
        return pocketForObject;
    }

    public void setPocketForObject(Object pocketForObject) {
        this.pocketForObject = pocketForObject;
    }
}
