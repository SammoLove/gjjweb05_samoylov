package com.getjavajob.training.web05.samoylove.lesson03;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Task1 {
    public static void main(String[] args) {
        for (String str : printPathsAsXML()) {
            System.out.println(str);
        }
    }

    public static List<String> printPathsAsXML() {
        Map<String, String> all = System.getenv();
        Set<String> keys = all.keySet();
        List<String> outputXML = new LinkedList<>();
        outputXML.add("<pathsSet>");
        for (String key : keys) {
            outputXML.add("\t<entry>");
            outputXML.add("\t\t<name>" + key + "</name>");
            outputXML.add("\t\t<value>" + all.get(key) + "</value>");
            outputXML.add("\t</entry>");
        }
        outputXML.add("</pathsSet>");
        return outputXML;
    }
}
