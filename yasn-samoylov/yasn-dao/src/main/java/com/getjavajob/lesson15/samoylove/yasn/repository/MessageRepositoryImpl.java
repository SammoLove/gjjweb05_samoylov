package com.getjavajob.lesson15.samoylove.yasn.repository;

import com.getjavajob.lesson15.samoylove.yasn.entity.Interlocutor;
import com.getjavajob.lesson15.samoylove.yasn.entity.Message;
import com.getjavajob.lesson15.samoylove.yasn.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Repository
public class MessageRepositoryImpl extends AbstractCommonRepository<Message> implements MessageRepository {
	@Override
	protected Class<Message> getEntityClass() {
		return null;
	}

	@SuppressWarnings("JpaQlInspection")
	@Override
	public Long getUnreadCount(User user) {
		String query = "SELECT COUNT(m) FROM Message m WHERE m.receiver = :id AND m.readed = false";
		TypedQuery<Long> typedQuery = entityManager.createQuery(query, Long.class);
		typedQuery.setParameter("id", user);
		return typedQuery.getSingleResult();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Interlocutor> getInterlocutors(User user) {
		// There is no UNION in JPQL, that's why will be 2 queries
		String query =
				"SELECT * FROM usert WHERE id IN " +
						"(SELECT user_id FROM message WHERE author_user_id = :id " +
						"UNION " +
						"SELECT author_user_id FROM message WHERE user_id = :id) ";
		Query typedQuery = entityManager.createNativeQuery(query, User.class);
		typedQuery.setParameter("id", user.getId());
		final List<User> interlocutorsUsers = (ArrayList<User>) typedQuery.getResultList();

		List<Interlocutor> interlocutors = new LinkedList<>();
		for (User interlocutorUser : interlocutorsUsers) {
			Interlocutor interlocutor = new Interlocutor();
			interlocutor.setId(interlocutorUser.getId());
			interlocutor.setName(interlocutorUser.getName());
			interlocutor.setNickname(interlocutorUser.getNickname());

			User tmpUser = new User();
			tmpUser.setId(interlocutor.getId());
			interlocutor.setUnreadMsgsCount(getUnreadCount(tmpUser));
			interlocutors.add(interlocutor);
		}
		return interlocutors;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Message> getCorrespondence(User user, Interlocutor interlocutor) {
		// There is no UNION in JPQL, that's why will be 2 queries
		String query =
				"SELECT * FROM message WHERE user_id = :id AND author_user_id = :id2 " +
						"UNION " +
						"SELECT * FROM message WHERE user_id = :id2 AND author_user_id = :id";
		Query typedQuery = entityManager.createNativeQuery(query, Message.class);
		typedQuery.setParameter("id", user.getId());
		typedQuery.setParameter("id2", interlocutor.getId());
		return (ArrayList<Message>) typedQuery.getResultList();
	}

	@Override
	public void createPrivate(int sender, int receiver, String message) {

		//query.setParameter("date", new java.util.Date(), TemporalType.DATE);

	}

	@Override
	public void createPublic(int senderId, int receiverId, String message) {

	}

	@SuppressWarnings("JpaQlInspection")
	@Override
	public void setReaded(int... msgId) {
		//todo WTF?
		int count = entityManager.createQuery("UPDATE Message SET area = 0").executeUpdate();
	}


	@SuppressWarnings("JpaQlInspection")
	@Deprecated
	public List<Message> getAllOf(User user) {
		String query = "SELECT m FROM Message m WHERE user_id = :id OR author_user_id = :id AND m.priv = true";
		TypedQuery<Message> typedQuery = entityManager.createQuery(query, Message.class);
		typedQuery.setParameter("id", user.getId());
		return typedQuery.getResultList();
	}

	@SuppressWarnings("JpaQlInspection")
	@Deprecated
	public List<Message> getInbox(User user) {
		String query = "SELECT m FROM Message m WHERE user_id = :id AND m.priv = true";
		TypedQuery<Message> typedQuery = entityManager.createQuery(query, Message.class);
		typedQuery.setParameter("id", user.getId());
		return typedQuery.getResultList();

        /*todo use criteria
        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Message> criteriaQuery = criteriaBuilder.createQuery(Message.class);
        Root<Message> fromMsg = criteriaQuery.from(Message.class);

        final Predicate p1 = criteriaBuilder.equal(fromMsg.get(Message_.receiver), user);
        final Predicate p2 = criteriaBuilder.equal(fromMsg.get(Message_.priv), Boolean.TRUE);

        criteriaQuery.where(p1, p2);

        TypedQuery<Message> q = entityManager.createQuery(criteriaQuery);
        return q.getResultList();*/
	}

	@SuppressWarnings("JpaQlInspection")
	@Deprecated
	public List<Message> getOutbox(User user) {
		String query = "SELECT m FROM Message m WHERE author_user_id = :id AND m.priv = true";
		TypedQuery<Message> typedQuery = entityManager.createQuery(query, Message.class);
		typedQuery.setParameter("id", user.getId());
		return typedQuery.getResultList();
	}
}