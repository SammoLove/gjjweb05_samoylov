package com.getjavajob.training.web05.samoylove.lesson16;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<T extends Comparable<T>> implements Iterable<T> {
    private int size;
    private Node<T> beforeFirst;
    private Node<T> afterLast;

    public DoublyLinkedList() {
        beforeFirst = new Node<>();
        afterLast = new Node<>();
        beforeFirst.next = afterLast;
        afterLast.prev = beforeFirst;
        size = 0;
    }

    public void set(int index, T elem) {
        Node<T> pointer = setPointer(index);
        pointer.data = elem;
    }

    public void add(T elem) {
        Node<T> last = new Node<>(afterLast.prev, elem, afterLast);
        afterLast.prev.next = last;
        afterLast.prev = last;
        size++;
    }

    public T get(int index) {
        Node<T> pointer = setPointer(index);
        return pointer == null ? null : pointer.data;
    }

    private Node<T> setPointer(int index) {
        Node<T> pointer = null;
        if (!isEmpty() && index < size) {
            if (index <= size/2) {
                pointer = beforeFirst;
                for (int i = 0; i <= index; i++) {
                    pointer = pointer.next;
                }
            } else {
                pointer = afterLast;
                for (int i = size-1; i >= index; i--) {
                    pointer = afterLast.prev;
                }
            }
        }
        return pointer;
    }

    public boolean isEmpty() {
        return beforeFirst.next == afterLast.prev;
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DoublyLinkedList. Size=").append(size).append(". {");

        Node<T> pointer = beforeFirst;
        for (int i = 0; i < size; i++) {
            pointer = pointer.next;
            sb.append(pointer.data).append(", ");
        }

        sb.delete(sb.length()-2, sb.length()).append('}');
        return sb.toString();
    }

    /* ========================= Iterator ============================= */
    @Override
    public Iterator<T> iterator() {
        return new Iter<>();
    }

    private class Node<E> {
        E data;
        Node<E> prev;
        Node<E> next;

        public Node() {
        }

        public Node(Node<E> prev, E data, Node<E> next) {
            this.prev = prev;
            this.data = data;
            this.next = next;
        }
    }

    private class Iter<S> implements Iterator<S> {
        private Node<S> pointer;
        private Node<S> next;
        private int currentIndex;

        @Override
        public boolean hasNext() {
            return currentIndex + 1 < size;
        }

        @Override
        public S next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            pointer = next;
            next = next.next;
            currentIndex++;
            return pointer.data;
        }

        @Override
        public void remove() {
            Node<S> lastNext = pointer.next;
            pointer.next.prev = pointer.prev;
            pointer.prev.next = pointer.next;
            if (next == pointer) {
                next = lastNext;
            } else {
                currentIndex--;
            }
        }
    }
}