package com.getjavajob.lesson15.samoylove.yasn.repository;

import com.getjavajob.lesson15.samoylove.yasn.entity.Country;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends DAO<Country>, CommonDBOperations<Country> {
}