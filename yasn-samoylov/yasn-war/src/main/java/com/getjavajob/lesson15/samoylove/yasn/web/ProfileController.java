package com.getjavajob.lesson15.samoylove.yasn.web;

import com.getjavajob.lesson15.samoylove.yasn.entity.City;
import com.getjavajob.lesson15.samoylove.yasn.entity.Country;
import com.getjavajob.lesson15.samoylove.yasn.entity.User;
import com.getjavajob.lesson15.samoylove.yasn.services.LocationService;
import com.getjavajob.lesson15.samoylove.yasn.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
public class ProfileController {
    private UserService userService;
    private LocationService locationService;
    private UserInfoSessionStorage userInfoSessionStorage;

    @Autowired
    public ProfileController(UserService userService, LocationService locationService, UserInfoSessionStorage userInfoSessionStorage) {
        this.userService = userService;
        this.locationService = locationService;
        this.userInfoSessionStorage = userInfoSessionStorage;
    }

    @RequestMapping(path = "/register", method = RequestMethod.GET)
    public String getRegisterPage() {
        return "register";
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ModelAndView register(HttpServletRequest req) throws ServletException, IOException {
        final User user = getUserFromRequest(req); //get fields from request and save it to User object
        Map<String, String> inputErrors = validateInput(req.getParameter("password2"), user);
        if (inputErrors != null) { //if form data filled wrong, return to register form
            final Map<String, Object> model = new HashMap<>(); //before returning, save filled fields
            final Iterable<Country> allCountries = locationService.returnAllCountries();
            model.put("allCountries", allCountries);
            model.put("savedFields", user);
            model.put("errors", inputErrors);
            return new ModelAndView("register", model);
        }
        // if no errors, proceed
        User newUser = userService.create(user);
        if (newUser == null || newUser.getId() < 0) {
            Map<String, Object> model = new HashMap<>();
            final Iterable<Country> allCountries = locationService.returnAllCountries();
            inputErrors = new HashMap<>();
            inputErrors.put("commonError", "Error while adding to DB");
            model.put("savedFields", user);
            model.put("allCountries", allCountries);
            model.put("errors", inputErrors);
            return new ModelAndView("register", model);
        } else { // ID ненулевой - юзер успешно создан
            // todo вместо мгновенного логина перевести пользователя на страницу логина с сообщением,
            // todo что регистрация произошла, и осталось только залогиниться.

            Map<String, String> msg = new HashMap<String, String>() {{
                put("errorMsg", "Register successful! Please login.");
                put("email", user.getEmail());
            }};
            return new ModelAndView("login" + user.getName(), msg);
        }
    }

    @RequestMapping(path = "/edit", method = RequestMethod.GET)
    public String getEdit(final HttpServletRequest req) {
        User loggedUser = userService.read(userInfoSessionStorage.getUserId());
        req.setAttribute("savedFields", loggedUser);

        final Iterable<Country> allCountries = locationService.returnAllCountries();

        final City birthCity = locationService.readCity(loggedUser.getBirthLocation());
        req.setAttribute("birthCity", birthCity);
        //final int birthCountryId = birthCity.getCountryId();
        //req.setAttribute("birthCountryId", birthCountryId);

        final City currCity = locationService.readCity(loggedUser.getCurrLocation());
        req.setAttribute("currCity", currCity);
        //final int currCountryId = currCity.getCountryId();
        //req.setAttribute("currCountryId", currCountryId);

        req.setAttribute("allCountries", allCountries);
        return "edit";
    }

    @RequestMapping(path = "/edit", method = RequestMethod.POST)
    public String edit(final HttpServletRequest req) throws ServletException, IOException {
        final User user = getUserFromRequest(req);
        final Map<String, String> errors = validateInput(req.getParameter("password2"), user);
        //TODO оживить!

        userService.update(user);
        req.removeAttribute("savedFields");
        req.removeAttribute("regErrorMessage");
        req.removeAttribute("allCountries");
        req.getSession().setAttribute("loggedUserName", user.getName());
        return "wall?=" + user.getId();
    }

    private Map<String, String> validateInput(String password2, User user) throws ServletException, IOException {
        // TODO Do checking in JS

        final String password = user.getPassword();
        final Map<String, String> errorMap = new HashMap<>();

        boolean incorrectFilled = false;
        if (user.getPassword() == null) {
            errorMap.put("name", "it is required field");
            incorrectFilled = true;
        }
        if (password2 == null) {
            errorMap.put("birth", "it is required field");
            incorrectFilled = true;
        }
        if (password.equals(password2)) {
            errorMap.put("password", "password mismatch");
            incorrectFilled = true;
        }
        if (user.getName() == null) {
            errorMap.put("name", "it is required field");
            incorrectFilled = true;
        }
        if (user.getBirth() == null) {
            errorMap.put("birth", "it is required field");
            incorrectFilled = true;
        }
        if (user.getEmail() == null) {
            errorMap.put("email", "it is required field");
            incorrectFilled = true;
        }
        return incorrectFilled ? errorMap : null;
    }


    private User getUserFromRequest(HttpServletRequest req) {
        final User user = new User();
        try {
            req.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        user.setName(req.getParameter("name"));
        user.setPassword(req.getParameter("password"));
        final String birth = req.getParameter("birth");
        if (birth != null && !birth.equals("")) {
            user.setBirth(Date.valueOf(birth));
        }
        user.setEmail(req.getParameter("email"));
        final String bcity = req.getParameter("bcity");
        if (bcity != null && !bcity.equals("")) {
            user.setBirthLocation(Integer.parseInt(bcity));
        }
        final String ccity = req.getParameter("ccity");
        if (ccity != null && !ccity.equals("")) {
            user.setCurrLocation(Integer.parseInt(ccity));
        }
        final String gender = req.getParameter("gender");
        if (gender != null) {
            user.setGender(Boolean.valueOf(gender));
        }
        return user;
    }
}