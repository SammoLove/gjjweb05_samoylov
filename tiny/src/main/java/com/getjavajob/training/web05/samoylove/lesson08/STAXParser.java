package com.getjavajob.training.web05.samoylove.lesson08;

import com.getjavajob.training.web05.samoylove.lesson06.Task2;

import java.io.File;
import java.util.Collection;
import java.util.Map;

import static com.getjavajob.training.web05.samoylove.lesson06.Task2.loadGoodsBySTAX;

public class STAXParser implements XmlParser {
    @Override
    public Map<String, Collection<Task2.Product>> parser(File XMLFile) {
        return loadGoodsBySTAX(XMLFile);
    }
}