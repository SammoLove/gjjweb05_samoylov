package com.getjavajob.lesson15.samoylove.yasn.repository;

import com.getjavajob.lesson15.samoylove.yasn.entity.TO;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public abstract class AbstractCommonRepository<T extends TO> implements DAO<T>, CommonDBOperations<T> {
    @PersistenceContext
    EntityManager entityManager;

    protected abstract Class<T> getEntityClass();

    @Override
    public T create(T newObject) {
        entityManager.persist(newObject);
        return newObject;
    }

    @Override
    public T read(int id) {
        return entityManager.find(getEntityClass(), id);
    }

    @Override
    public void update(T object) {
        entityManager.merge(object);
    }

    @Override
    public void delete(T object) {
        entityManager.remove(object);
    }

    @Override
    public void deleteById(int id) {
        entityManager.remove(read(id));
    }

    @Override
    public List<T> returnAll() {
        TypedQuery<T> query = entityManager.createQuery("SELECT o FROM " + getEntityClass().getName() + " o", getEntityClass());
        return query.getResultList();
    }

    @Override
    public Integer count() {
        TypedQuery<Integer> query = entityManager.createQuery(
                "SELECT COUNT(*) FROM " + getEntityClass().getName() + " c", Integer.class);
        return query.getSingleResult();
    }

    /*@Override
    public boolean exist(int id) {
        final T object = entityManager.find(getEntityClass(), id);
        return object != null;
    }*/
}