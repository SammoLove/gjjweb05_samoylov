package com.getjavajob.training.web05.samoylove.lesson04;

import java.util.LinkedList;
import java.util.List;

public class ClassExample implements Cloneable {
    /**
     * @Transient fields marked with this annotation must not be copied
     * @Shallow 1. fields of reference type marked with this annotations must be copied using shallow copy rather than deep copy
     * 2. classes marked by this annotation tell that all reference-typed fields must be copied using shallow copy
     * 3. if field is not of reference type, log a warning to std err
     * <p/>
     * If both of these annotations are found to be applied to the same field, then log a warning to std err
     */

    public static final int CONST = 1;
    public static int b;
    public int[] h;
    private int a;
    private Integer c;
    private String d;
    @Task2.Transient
    private String dTransient;
    @Task2.Shallow
    private String dShallow;
    @Task2.Shallow
    @Task2.Transient
    private String dTransientShallow;
    /**
     * Objects inside collections and maps must be copied as well as collection object
     * itself given that the field is not marked with @Shallow.
     * Think twice about how would you implement collection copy?
     * Would you simply copy its structure or there is another way?
     */
    private List<?> f;
    private List g;
    private Double[] i;

    public ClassExample() {
    }

    public ClassExample(int a, Integer c, String d, String dTransient, String dShallow, String dTransientShallow, List<?> f, List g) {
        this.a = a;
        this.c = c;
        this.d = d;
        this.dTransient = dTransient;
        this.dShallow = dShallow;
        this.dTransientShallow = dTransientShallow;
        this.f = f;
        this.g = g;
    }

    public static int getCONST() {
        return CONST;
    }

    public static int getB() {
        return b;
    }

    public static void setB(int b) {
        ClassExample.b = b;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClassExample{");
        sb.append("a=").append(a);
        sb.append(", c=").append(c);
        sb.append(", d='").append(d).append('\'');
        sb.append(", dTransient='").append(dTransient).append('\'');
        sb.append(", dShallow='").append(dShallow).append('\'');
        sb.append(", dTransientShallow='").append(dTransientShallow).append('\'');
        sb.append(", f=").append(f);
        sb.append(", g=").append(g);
        sb.append('}');
        return sb.toString();
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public Integer getC() {
        return c;
    }

    public void setC(Integer c) {
        this.c = c;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getdTransient() {
        return dTransient;
    }

    public void setdTransient(String dTransient) {
        this.dTransient = dTransient;
    }

    public String getdShallow() {
        return dShallow;
    }

    public void setdShallow(String dShallow) {
        this.dShallow = dShallow;
    }

    public String getdTransientShallow() {
        return dTransientShallow;
    }

    public void setdTransientShallow(String dTransientShallow) {
        this.dTransientShallow = dTransientShallow;
    }

    public List<?> getF() {
        return f;
    }

    public void setF(LinkedList<?> f) {
        this.f = f;
    }

    public void setF(List<?> f) {
        this.f = f;
    }

    public List getG() {
        return g;
    }

    public void setG(List g) {
        this.g = g;
    }

    public int[] getH() {
        return h;
    }

    public void setH(int[] h) {
        this.h = h;
    }

    public Double[] getI() {
        return i;
    }

    public void setI(Double[] i) {
        this.i = i;
    }

    /**
     * Fields can be enums.
     */
    public enum e {
        ERROR, MISTAKE, FAULT
    }
}
