package com.getjavajob.training.web05.samoylove.lesson11;

public class Chat {
    public static void main(String[] args) {
        if (System.getProperty("server").equals("true")) {
            new Server(Integer.valueOf(System.getProperty("port")));
        } else {
            new Client(Integer.valueOf(System.getProperty("port")), System.getProperty("host"), System.getProperty("user"));
        }
    }
}