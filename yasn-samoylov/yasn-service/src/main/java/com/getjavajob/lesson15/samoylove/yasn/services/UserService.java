package com.getjavajob.lesson15.samoylove.yasn.services;

import com.getjavajob.lesson15.samoylove.yasn.entity.User;
import com.getjavajob.lesson15.samoylove.yasn.repository.SearchParams;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
	User create(User user);

	User read(int id);

	void update(User user);

	int count();

	List<User> search(SearchParams searchParams);

	User searchByNickname(String nickname);

	boolean isFriend(int who, int forWho);
}