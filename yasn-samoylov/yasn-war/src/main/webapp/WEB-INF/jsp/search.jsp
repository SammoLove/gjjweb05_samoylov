<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Advanced search</title>
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/wall.css" />"/>
	<script defer src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script defer src="<c:out value="${pageContext.request.contextPath}"/>/js/search.js"></script>
	<script defer src="<c:out value="${pageContext.request.contextPath}"/>/js/citiesLoad.js"></script>
</head>
<body>
<div id="header">
	<%@ include file="/WEB-INF/jsp/header.jsp" %>
</div>
<div class="wrap left">
	<div class="page_layout">
		<div class="content">
			<%--если уже что-то ищут, скрыть поля ввода поисковых параметров--%>

			<form class="search-form" id="form" action="#" method="get">
				<fieldset>
					<legend>Not required all fields, would be enough to fill any one</legend>
					<input type="hidden" name="type" value="adv"/>

					<label for="partName">Name</label>
					<input class="search-form" id="partName" name="partName"
						   value="<c:out value="${param.partName}"/>"/>
					<br>

					<label for="m">M</label><input type="radio" id="m" name="gender" value="true"
												   <c:if test="${param.gender == 'true'}">checked</c:if> />
					<label for="f">F</label><input type="radio" id="f" name="gender" value="false"
												   <c:if test="${param.gender == 'false'}">checked</c:if> />
					<br>
					<label for="minAge">Age from</label>
					<input type="number" style="width: 3em;" name="minAge" id="minAge"
						   value="<c:out value="${param.minAge}"/>"/>
					<label for="maxAge">to</label>
					<input type="number" style="width: 3em;" name="maxAge" id="maxAge"
						   value="<c:out value="${param.maxAge}"/>"/>
					<br>

					<label for="birthCountry">Birth Country</label>
					<select name="birthCountry" id="birthCountry">
						<option value=""></option>
						<%--@elvariable id="allCountries" type="java.util.List"--%>
						<c:forEach var="country" items="${allCountries}" varStatus="ctr">
							<option value="<c:out value="${country.id}"/>"><c:out value="${country.name}"/></option>
						</c:forEach>
						<c:out value="${param.bcountry}"/>
					</select>
					or <input id="mybс" name="mybс" type="checkbox"/><label for="mybс">like mine</label>
					<br>

					<label for="birthCity">Birth City</label>
					<select name="birthCity" id="birthCity">
					</select>
					or <input id="mybсt" name="mybсt" type="checkbox"/><label for="mybсt">like mine</label>
					<br>

					<label for="currCountry">Current Country</label>
					<select name="currCountry" id="currCountry">
						<option value=""></option>
						<c:forEach var="country" items="${allCountries}" varStatus="ctr">
							<option value="<c:out value="${country.id}"/>"><c:out value="${country.name}"/></option>
						</c:forEach>
					</select>
					or <input id="mycс" name="mycс" type="checkbox"/><label for="mycс">like mine</label>
					<br>

					<label for="currCity">Current City</label>
					<select name="currCity" id="currCity">
					</select>
					or <input id="mycсt" name="mycсt" type="checkbox"/><label for="mycсt">like mine</label>
					<br>
					<input id="submit" type="submit" name="submit" value="Search!"/>
					<button id="submitButton" onclick="showNewResults()">Ajax search</button>
				</fieldset>
			</form>
			<div id="searchResults"></div>

			<button onclick="showForm()" id="showButt">New search</button>

			<%--@elvariable id="searchResults" type="java.util.List"--%>
			<c:if test="${not empty searchResults}">
				<script>
					var form = document.getElementById("form");
					form.style.display = "none";
				</script>
				<%@ include file="/WEB-INF/jsp/search-results.jsp" %>
			</c:if>

		</div>

		<div class="left_menu">
			<%@ include file="/WEB-INF/jsp/leftcolumn.jsp" %>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div id="footer">
	<%@ include file="/WEB-INF/jsp/footer.jsp" %>
</div>
</body>
</html>