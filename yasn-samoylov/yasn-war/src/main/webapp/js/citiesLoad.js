$(document).ready(function (){
	$("#currCountry").change(function (){
		loadCities("#birthCity", "#birthCountry");
		loadCities("#currCity", "#currCountry");
	});
});

function loadCities(citySelector, countrySelector){
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	xmlhttp.onreadystatechange = function (){
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			$(citySelector).append("<option value=''></option>" + xmlhttp.responseText);
		}
	};
	xmlhttp.open("GET", "/getCities/" + $(countrySelector).val(), true);
	xmlhttp.send();
}