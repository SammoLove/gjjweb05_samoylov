package com.getjavajob.training.web05.samoylove.lesson03;

import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;

public class Task1Test {

    @Test
    public void testPrintPathsAsXML() {
        Assert.assertTrue(Task1.printPathsAsXML().size() >= 6);
        Assume.assumeTrue(Task1.printPathsAsXML().contains("\t\t<name>Path</name>"));
        Assert.assertEquals("</pathsSet>", Task1.printPathsAsXML().get(Task1.printPathsAsXML().size() - 1));
    }
}