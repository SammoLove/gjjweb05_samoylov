package com.getjavajob.lesson15.samoylove.yasn.web;

import com.getjavajob.lesson15.samoylove.yasn.entity.Avatar;
import com.getjavajob.lesson15.samoylove.yasn.entity.User;
import com.getjavajob.lesson15.samoylove.yasn.services.LocationService;

public class UserForSearch {
	private String id;
	private String name;
	private String nickname;
	private String gender;
	private String age;
	private String birthCountry;
	private String birthCity;
	private String currCountry;
	private String currCity;
	private Avatar avatar;

	private LocationService locationService;

	public UserForSearch() {
	}

	public UserForSearch(final User user, final LocationService locationService) {
		if (user == null) return;

		final int id = user.getId();
		this.id = id > 0 ? String.valueOf(id) : null;

		this.name = user.getName();
		this.nickname = user.getNickname();

		final Boolean gender = user.getGender();
		this.gender = gender != null ? (gender ? "male" : "female") : null;

		final int age = user.getAge();
		this.age = id > 0 ? String.valueOf(age) : null;

		final int birthLocation = user.getBirthLocation();
		this.birthCountry = birthLocation > 0 ? locationService.readCity(birthLocation).getCountry().getName() : null;
		this.birthCity = birthLocation > 0 ? locationService.readCity(birthLocation).getName() : null;

		final int currLocation = user.getCurrLocation();
		this.currCountry = currLocation > 0 ? locationService.readCity(currLocation).getCountry().getName() : null;
		this.currCity = currLocation > 0 ? locationService.readCity(currLocation).getName() : null;
		this.avatar = user.getAvatar();
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getBirthCountry() {
		return birthCountry;
	}

	public void setBirthCountry(String birthCountry) {
		this.birthCountry = birthCountry;
	}

	public String getBirthCity() {
		return birthCity;
	}

	public void setBirthCity(String birthCity) {
		this.birthCity = birthCity;
	}

	public String getCurrCountry() {
		return currCountry;
	}

	public void setCurrCountry(String currCountry) {
		this.currCountry = currCountry;
	}

	public String getCurrCity() {
		return currCity;
	}

	public void setCurrCity(String currCity) {
		this.currCity = currCity;
	}

	public Avatar getAvatar() {
		return avatar;
	}

	public void setAvatar(Avatar avatar) {
		this.avatar = avatar;
	}
}