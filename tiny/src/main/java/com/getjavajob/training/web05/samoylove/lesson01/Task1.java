package com.getjavajob.training.web05.samoylove.lesson01;

import java.util.ArrayList;
import java.util.List;

public class Task1 {
    public static List<String> reverseList(List<String> input) {
        List<String> output = new ArrayList<>();
        for (String s : input) {
            if (s != null) {
                StringBuilder inverted = new StringBuilder(s);
                output.add(inverted.reverse().toString());
            }
        }
        return output;
    }
}