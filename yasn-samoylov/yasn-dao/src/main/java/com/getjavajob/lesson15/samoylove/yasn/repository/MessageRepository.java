package com.getjavajob.lesson15.samoylove.yasn.repository;

import com.getjavajob.lesson15.samoylove.yasn.entity.Interlocutor;
import com.getjavajob.lesson15.samoylove.yasn.entity.Message;
import com.getjavajob.lesson15.samoylove.yasn.entity.User;

import java.util.List;

public interface MessageRepository extends DAO<Message>, CommonDBOperations<Message> {
	List<Interlocutor> getInterlocutors(User user);

	List<Message> getCorrespondence(User user, Interlocutor interlocutor);

    void createPrivate(int sender, int receiver, String message);

    void createPublic(int senderId, int receiverId, String message);

	void setReaded(int... msgId);

	Long getUnreadCount(User user);

	@Deprecated
	List<Message> getInbox(User user);

	@Deprecated
	List<Message> getOutbox(User user);

	@Deprecated
	List<Message> getAllOf(User user);
}
