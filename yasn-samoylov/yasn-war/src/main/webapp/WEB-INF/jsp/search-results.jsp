<div id="search-results">
    <%--@elvariable id="searchResults" type="java.util.List"--%>
    <c:set var="counter" value="1" scope="page"/>
    <c:forEach var="user" items="${searchResults}" varStatus="ctr">
        <div class="searched-user">
            <c:out value="${counter}"/>. <a href="wall/id<c:out value="${user.id}"/>"><c:out value="${user.name}"/></a>
            <c:out value="${user.age}"/> years,
            from <c:out value="${user.currCity}"/>, <c:out value="${user.currCountry}"/>
        </div>
        <c:set var="counter" value="${counter + 1}" scope="page"/>
    </c:forEach>
</div>