package com.getjavajob.training.web05.samoylove.lesson03;

import org.junit.Assert;
import org.junit.Test;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

public class Task3EmplCollsTest {
    static Task3EmplColls all;
    Collection<Manager> rightAllManagers;
    Employee employee1;
    Employee employee2;
    Employee employee3;
    LinkedList<Employee> rightSub1;
    LinkedList<Employee> rightSub2;

    public void init() {
        employee1 = new Employee("Вася", "ул. Пушкина", "охранник");
        employee2 = new Employee("Петя", "ул. Ленина", "продавец");
        employee3 = new Employee("Третий", "ул. Третьего Мая", "футболист");
        Manager manager1 = new Manager("Сергей", "пр. Победы");
        Manager manager2 = new Manager("Дмитрий", "пр. Мира");
        // wrappering...
        manager1.addEmployee(employee1);
        manager1.addEmployee(employee2);
        manager1.addEmployee(employee3);
        manager2.addEmployee(employee1);
        manager2.addEmployee(employee3);
        HashMap<String, Employee> employeesMap = new HashMap<>();
        employeesMap.put(employee1.getName(), employee1);
        employeesMap.put(employee2.getName(), employee2);
        employeesMap.put(employee3.getName(), employee3);
        HashMap<String, Manager> managersMap = new HashMap<>();
        managersMap.put(manager1.getName(), manager1);
        managersMap.put(manager2.getName(), manager2);
        all = new Task3EmplColls();
        all.setEmployeesMap(employeesMap);
        all.setManagersMap(managersMap);

        rightAllManagers = new LinkedList<>();
        rightAllManagers.add(manager1);
        rightAllManagers.add(manager2);
        rightSub1 = new LinkedList<>();
        rightSub1.add(employee1);
        rightSub1.add(employee3);
        rightSub2 = new LinkedList<>();
        rightSub2.add(employee1);
        rightSub2.add(employee3);
        rightSub2.add(employee2);
        //end of data filling :)
    }

    @Test
    public void testGetAllManagers() {
        init();
        Collection<Manager> allManagers = all.getAllManagers();
        Assert.assertEquals(rightAllManagers, allManagers);
    }

    @Test
    public void testGetSubordinates() {
        init();
        try { // if such manager is not manager
            all.getSubordinates("Вася");
            Assert.fail("Wrong logic, should not find it, must throw ex");
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(true);
        }
        try { // if such manager don't exist
            all.getSubordinates("Нет такого имени");
            Assert.fail("Wrong logic, should not find it, must throw ex");
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(true);
        }
        try { // right work
            LinkedList<Employee> sub = all.getSubordinates("Дмитрий");
            Assert.assertEquals(rightSub1, sub);
        } catch (IllegalArgumentException e) {
            Assert.fail("Manager not found, but should");
        }
    }

    @Test
    public void testAddSubordinates() {
        init();
        try { // if such employee already exist, don't accept
            all.addSubordinate("Дмитрий", employee1);
            Assert.fail("Wrong logic");
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(true);
        }
        try { // if such manager is not manager
            all.addSubordinate("Вася", employee1, employee2, employee3);
            Assert.fail("Wrong logic");
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(true);
        }
        try { // right work
            all.addSubordinate("Дмитрий", employee2);
            LinkedList<Employee> sub = all.getSubordinates("Дмитрий");
            Assert.assertEquals(rightSub2, sub);
        } catch (IllegalArgumentException e) {
            Assert.fail("Manager not found, but should");
        }
    }

    @Test
    public void testRemoveEmployee() {
        init();
        try { // trying to remove manager
            all.removeEmployee("Такого нет", "Сергей");
            Assert.fail("Wrong logic");
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(true);
        }
        try { // trying to remove manager
            all.removeEmployee("Дмитрий", "Сергей");
            Assert.fail("Wrong logic");
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(true);
        }
        try { // trying to remove employee, that don't exist
            all.removeEmployee("Дмитрий", "Такого нет");
            Assert.fail("Wrong logic");
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(true);
        }
        try { // right work
            all.removeEmployee("Дмитрий", "Вася");
            Assert.assertTrue(true);
        } catch (IllegalArgumentException e) {
            Assert.fail("removeEmployee Fail!");
        }
    }
}