package com.getjavajob.lesson15.samoylove.yasn.repository;

import com.getjavajob.lesson15.samoylove.yasn.entity.City;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends DAO<City>, CommonDBOperations<City> {
}