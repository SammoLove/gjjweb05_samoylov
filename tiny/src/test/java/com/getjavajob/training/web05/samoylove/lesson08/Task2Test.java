package com.getjavajob.training.web05.samoylove.lesson08;

import com.getjavajob.training.web05.samoylove.lesson06.Task2;
import org.junit.Test;

import java.io.File;
import java.util.Collection;
import java.util.Map;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class Task2Test {
    File XMLFile = new File("src\\main\\java\\com\\getjavajob\\training\\web05\\samoylove\\lesson02\\Task2.xml");
    Task2.Product oneOf = new Task2.Product("Lexus CT", 1, 45000.0);

    @Test
    public void testXmlParserFactoryWorks() {
        //final XmlParserFactory xmlParserFactory = new XmlParserFactory();
        final XmlParser xmlParser = XmlParserFactory.newXmlParser();
        final Map<String, Collection<Task2.Product>> priceList = xmlParser.parser(XMLFile);

        assertTrue(priceList.get("Comfort").contains(oneOf));
        assertSame(2, priceList.get("Economy").size());
        assertSame(2, priceList.keySet().size());
        assertSame(2, priceList.size());
    }

    @Test
    public void testXmlParserFactoryIsStrategy() {
        // check with reflection, object of which type was created with different size of available memory
    }

    @Test
    public void testXmlParserFactoryIsSingletonAndThreadSafe() {
        //I don't know hot to check it
    }
}
