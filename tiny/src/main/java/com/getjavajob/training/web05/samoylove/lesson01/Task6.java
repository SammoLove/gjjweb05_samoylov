package com.getjavajob.training.web05.samoylove.lesson01;

import java.util.Set;

public class Task6 {
    public static <T> Set<T> intersectionSets(Set<T> set1, Set<T> set2) {
        if (set1 != null && set2 != null && set1.retainAll(set2)) {
            return set1;
        }
        return null;
    }
}