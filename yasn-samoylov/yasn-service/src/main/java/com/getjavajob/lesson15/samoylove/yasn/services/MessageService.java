package com.getjavajob.lesson15.samoylove.yasn.services;

import com.getjavajob.lesson15.samoylove.yasn.entity.Interlocutor;
import com.getjavajob.lesson15.samoylove.yasn.entity.Message;
import com.getjavajob.lesson15.samoylove.yasn.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MessageService {
	List<Interlocutor> getInterlocutors(User user);

	List<Message> getCorrespondence(User user, Interlocutor interlocutor);

	Integer getUnreadCount(User user);

	void sendPrivate(User sender, User receiver, String message);

	void sendPublic(User sender, User receiver, String message);

	void markAsRead(int... msgId);
}