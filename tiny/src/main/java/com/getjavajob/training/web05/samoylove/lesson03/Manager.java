package com.getjavajob.training.web05.samoylove.lesson03;

import java.util.LinkedList;
import java.util.Objects;

public class Manager extends Person {
    private LinkedList<Employee> employeesUnder;

    public Manager(String name, String homeAddress) {
        super(name, homeAddress);
        employeesUnder = new LinkedList<>();
    }

    public boolean addEmployee(Employee employee) {
        return employeesUnder.add(employee);
    }

    public boolean removeEmployee(Employee employee) {
        return employeesUnder.remove(employee);
    }

    public LinkedList<Employee> getEmployeesUnder() {
        return employeesUnder;
    }

    @Override
    public boolean equals(Object o) {
        Manager manager = (Manager) o;
        return this == o || getName().equals(manager.getName()) && getHomeAddress().equals(manager.getHomeAddress()) && employeesUnder.equals(manager.employeesUnder);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getEmployeesUnder());
    }

    @Override
    public Manager clone() throws CloneNotSupportedException {
        Manager copy = (Manager) super.clone();
        copy.employeesUnder = new LinkedList<>();
        for (int i = 0; i < employeesUnder.size(); i++) {
            copy.employeesUnder.add(employeesUnder.get(i).clone());
        }
        return copy;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Manager{");
        sb.append("name='").append(getName()).append('\'');
        sb.append(", homeAddress='").append(getHomeAddress()).append('\'');
        sb.append(", employeesUnder=").append(employeesUnder);
        sb.append('}');
        return sb.toString();
    }
}