package com.getjavajob.lesson15.samoylove.yasn.web;

import com.getjavajob.lesson15.samoylove.yasn.entity.Avatar;
import com.getjavajob.lesson15.samoylove.yasn.entity.MultipartFileContainer;
import com.getjavajob.lesson15.samoylove.yasn.entity.User;
import com.getjavajob.lesson15.samoylove.yasn.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/ava")
public class AvatarController {
    @Autowired
    UserService userService;

    @Autowired
    UserInfoSessionStorage userInfoSessionStorage;

    /*@Autowired
    FileValidator fileValidator;*/

    /*@InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(fileValidator);
    }*/

    @RequestMapping(path = "/upload", method = RequestMethod.POST)
    public String uploadFile(@ModelAttribute final MultipartFileContainer multipartFileContainer,
                             final BindingResult result) throws IOException {
        int loggedUserId = userInfoSessionStorage.getUserId();

        if (result.hasErrors()) {
            return "/wall/id" + loggedUserId;
        }
        final MultipartFile avatarImageFile = multipartFileContainer.getMultipartFile();
        final Avatar avatar = new Avatar();
        final User user = userService.read(loggedUserId);

        avatar.setMimeType(avatarImageFile.getContentType());
        avatar.setFile(avatarImageFile.getBytes());
        avatar.setId(user.getId()); //!

        user.setAvatar(avatar);
        userService.update(user);

        return "redirect:/wall/id" + loggedUserId;
    }

    @RequestMapping(path = "/{userId}", method = RequestMethod.GET)
    public void getAvatar(final HttpServletResponse resp,
                          @PathVariable int userId,
                          @RequestParam(name = "down", required = false) Boolean isDownload) {
        final User user = userService.read(userId);
        final Avatar avatar = user.getAvatar();
        if (avatar != null) {
            resp.setStatus(200);
            String extension;
            switch (avatar.getMimeType()) {
                case "image/jpeg":
                    extension = "jpg";
                    break;
                case "image/pjpeg":
                    extension = "jpg";
                    break;
                case "image/png":
                    extension = "png";
                    break;
                case "image/gif":
                    extension = "gif";
                    break;
                default:
                    extension = "jpg";
            }
            //определяем тип файла
            //записываем аватарку в сессионный scope
            //переадресуем на /ava/ava123.jpg

            if (isDownload != null) {
                avatar.setMimeType("application/octet-stream");
            }
            userInfoSessionStorage.setPocketForObject(avatar);
            try {
                resp.sendRedirect("/ava/ava" + userId + '.' + extension);
            } catch (IOException e) {
                Logger logger = LogManager.getRootLogger();
                logger.error("Error in getAvatar while resp.sendRedirect: " + e.getMessage());
                e.printStackTrace();
            }
        } else {
            try {
                resp.sendRedirect("/img/default-photo.jpg");
            } catch (IOException e) {
                e.printStackTrace();
            }
            //resp.setStatus(404);
        }
    }

    @RequestMapping(path = "/ava{userId}.*", method = RequestMethod.GET)
    public void getAvatarFile(final HttpServletResponse resp) {
        final Avatar avatar = (Avatar) userInfoSessionStorage.getPocketForObject();
        userInfoSessionStorage.setPocketForObject(null);
        if (avatar != null) {
            resp.setStatus(200);
            resp.setContentType(avatar.getMimeType());
            resp.setContentLength(avatar.getFile().length);
            try {
                final ServletOutputStream outputStream = resp.getOutputStream();
                outputStream.write(avatar.getFile());
            } catch (IOException e) {
                Logger logger = LogManager.getRootLogger();
                logger.error("Error in getAvatar while resp.getOutputStream(): " + e.getMessage());
                e.printStackTrace();
            }
        } else {
            resp.setStatus(404);
        }
    }
}