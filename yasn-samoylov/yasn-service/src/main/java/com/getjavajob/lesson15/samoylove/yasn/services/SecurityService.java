package com.getjavajob.lesson15.samoylove.yasn.services;

import com.getjavajob.lesson15.samoylove.yasn.entity.SecuritySetting;
import org.springframework.stereotype.Service;

@Service
public interface SecurityService {
	SecuritySetting create(SecuritySetting securitySetting);

	SecuritySetting read(int id);

	void update(SecuritySetting securitySetting);
}