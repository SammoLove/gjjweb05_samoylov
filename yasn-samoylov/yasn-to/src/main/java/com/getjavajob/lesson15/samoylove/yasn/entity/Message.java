package com.getjavajob.lesson15.samoylove.yasn.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Time;

@Entity
public class Message extends TO {
    @Id
    @SequenceGenerator(name = "msg_id_seq", sequenceName = "msg_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "msg_id_seq")
    private int id;

    @NotNull
    private Time time;

    @Column(name = "body")
    private String message;

    //@JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User receiver;

    //@JsonIgnore
    @ManyToOne
    @JoinColumn(name = "author_user_id")
    private User sender;

    @NotNull
    private boolean readed;

    //private Avatar file;

    @NotNull
    private boolean priv;


    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public boolean isReaded() {
        return readed;
    }

    public void setReaded(boolean readed) {
        this.readed = readed;
    }

    public boolean isPriv() {
        return priv;
    }

    public void setPriv(boolean priv) {
        this.priv = priv;
    }
}