package com.getjavajob.lesson15.samoylove.yasn.services;

import com.getjavajob.lesson15.samoylove.yasn.entity.User;
import com.getjavajob.lesson15.samoylove.yasn.repository.SearchParams;
import com.getjavajob.lesson15.samoylove.yasn.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
	private final UserRepository userRepository;

	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	@Transactional
	public User create(User user) {
		return userRepository.create(user);
	}

	@Override
	@Transactional
	public User read(int id) {
		return userRepository.read(id);
	}

	@Override
	@Transactional
	public void update(User user) {
		userRepository.update(user);
	}

	@Override
	@Transactional
	public int count() {
		return userRepository.count();
	}

	@Override
	@Transactional
	public List<User> search(SearchParams searchParams) {
		return userRepository.searchByParams(searchParams);
	}

	@Override
	@Transactional
	public User searchByNickname(String nickname) {
		return userRepository.findByNickname(nickname);
	}

	@Override
	@Transactional
	public boolean isFriend(int who, int forWho) {
		//todo
		return false;
	}
}